package com.sejel.monitor.business.helper;

import org.codehaus.jackson.annotate.JsonProperty;

public class BasicRequest {
    @JsonProperty("Input")
    private Input input;

    public BasicRequest() {
    }

    public BasicRequest(Input input) {
        this.input = input;
    }

    public Input getInput() {
        return input;
    }

    public void setInput(Input input) {
        this.input = input;
    }
}
