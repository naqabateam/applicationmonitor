package com.sejel.monitor.business.helper;

import org.codehaus.jackson.annotate.JsonProperty;

public class BasicResponse {

    @JsonProperty("Result")
    private Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
