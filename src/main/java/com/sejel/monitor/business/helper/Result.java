package com.sejel.monitor.business.helper;

import org.codehaus.jackson.annotate.JsonProperty;

public class Result {

    @JsonProperty("ERROR_CODE")
    private Integer errorCode;

    @JsonProperty("ERROR_DESCRIPTION_LA")
    private String errorDescriptionLa;

    @JsonProperty("ERROR_DESCRIPTION_AR")
    private String errorDescriptionAr;

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescriptionLa() {
        return errorDescriptionLa;
    }

    public void setErrorDescriptionLa(String errorDescriptionLa) {
        this.errorDescriptionLa = errorDescriptionLa;
    }

    public String getErrorDescriptionAr() {
        return errorDescriptionAr;
    }

    public void setErrorDescriptionAr(String errorDescriptionAr) {
        this.errorDescriptionAr = errorDescriptionAr;
    }
}
