package com.sejel.monitor.business.helper;

import org.codehaus.jackson.annotate.JsonProperty;

public class Input {
    @JsonProperty("RequestID")
    private Long requestID;

    public Input() {
    }

    public Input(Long requestID) {
        this.requestID = requestID;
    }

    public Long getRequestID() {
        return requestID;
    }

    public void setRequestID(Long requestID) {
        this.requestID = requestID;
    }
}
