package com.sejel.monitor.business.helper;


import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Named
public class ExcelManager {

    private ResourceBundle bundle;

    private Map<String, String> data = new HashMap<>();

    public void writeSheet(List<String> headers, List<List<String>> data, List<byte[]> attachments,String flag) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = null;
        if (data.size() == 1)
            sheet = workbook.createSheet("Driver Card");
        else if (data.size() > 1)
            sheet = workbook.createSheet("Drivers Card");
        sheet.getCTWorksheet().getSheetViews().getSheetViewArray(0).setRightToLeft(true);
        Row header = sheet.createRow(0);
        for (int counter = 0; counter < headers.size(); counter++) {
            Cell headerCell = header.createCell(counter);
            headerCell.setCellValue(headers.get(counter));
            sheet.autoSizeColumn(counter);
        }


        for (int counter = 0; counter < data.size(); counter++) {
            Row information = sheet.createRow(counter + 1);
            List<String> rowData = data.get(counter);
            for (int cellCounter = 0; cellCounter < rowData.size(); cellCounter++) {
                Cell informationCell = information.createCell(cellCounter);
                informationCell.setCellValue(rowData.get(cellCounter));
            }
        }


        try {
            String fileName = null;
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = facesContext.getExternalContext();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            ZipOutputStream zipOutputStream = new ZipOutputStream(externalContext.getResponseOutputStream());

            if (attachments.isEmpty()) {

                if (data.size() == 1)
                    fileName = "attachment; filename=Card.xlsx";
                else if (data.size() > 1) {
                    fileName = "attachment; filename=Cards.xlsx";

                }
                externalContext.setResponseContentType("application/vnd.ms-excel");
                externalContext.setResponseHeader("Content-Disposition", fileName);
                workbook.write(externalContext.getResponseOutputStream());
                workbook.close();
            } else {
                if (data.size() == 1)
                    fileName = "attachment; filename=Card.ZIP";
                else if (data.size() > 1) {
                    fileName = "attachment; filename=Cards.ZIP";

                }
                externalContext.setResponseContentType("application/zip");
                externalContext.setResponseHeader("Content-Disposition", fileName);
                workbook.write(stream);
                workbook.close();
                zipOutputStream.putNextEntry(new ZipEntry("Card.xlsx"));
                zipOutputStream.write(stream.toByteArray());
                zipOutputStream.closeEntry();

                for (int counter = 0; counter < attachments.size(); counter++) {
                    List<String> row = data.get(counter);
                    if(!flag.equals("driver"))
                    zipOutputStream.putNextEntry(new ZipEntry(row.get(0) + ".jpg"));
                    else
                        zipOutputStream.putNextEntry(new ZipEntry(row.get(row.size()-1) + ".jpg"));
                    zipOutputStream.write(attachments.get(counter));
                    zipOutputStream.closeEntry();
                }
                zipOutputStream.close();
            }


            facesContext.responseComplete();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
