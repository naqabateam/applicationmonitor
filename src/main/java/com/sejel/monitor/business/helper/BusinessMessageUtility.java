package com.sejel.monitor.business.helper;

import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Locale;
import java.util.ResourceBundle;
@Named
public class BusinessMessageUtility implements Serializable {



    public String getValueByKey(String language, String key) {
        FacesContext context = FacesContext.getCurrentInstance();
        Locale locale = new Locale(language);
        ResourceBundle bundle = ResourceBundle.getBundle("localeResources", locale);
        String value = bundle.getString(key);
        return value;
    }
}