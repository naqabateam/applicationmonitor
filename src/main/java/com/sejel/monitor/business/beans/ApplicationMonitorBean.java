package com.sejel.monitor.business.beans;

import com.sejel.monitor.business.parentBeans.AbstractFacadeWrapper;
import com.sejel.monitor.persistence.entity.AmGroups;
import com.sejel.monitor.persistence.entity.AmKpis;
import com.sejel.monitor.persistence.entity.AmSystems;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Stateful
public class ApplicationMonitorBean extends AbstractFacadeWrapper<AmSystems> {
    @PersistenceContext(unitName = "MainUnit")
    private EntityManager em;

    public ApplicationMonitorBean() {
        super(AmSystems.class);
    }


    public List<AmKpis> getKpisForGroup(Long Id) {
        return em.createQuery("select s from AmKpis s where s.kpiEventsCount > 0 and s.kpiState=0 and s.kpiGrpId = :grp order by s.kpiId", AmKpis.class)
                .setParameter("grp", Id)
                .getResultList();
    }


    public synchronized List<AmSystems> refreshDashBoard() {
        List<AmKpis> KpisList = em.createQuery("select s from AmKpis s where s.kpiState = 0 and s.group.grpState = 0 and s.group.system.sysState = 0", AmKpis.class).getResultList();

        for (AmKpis temp : KpisList) {
            if (checkTime(temp)) {
                temp.setKpiEventsCount(Integer.valueOf(String.valueOf(em.createNativeQuery(temp.getKpiCheckQuery()).getSingleResult())));
                temp.setKpiLastRefreshTimestamp(new Date());
                em.merge(temp);
            }
        }

        List<AmSystems> systemsList = em.createQuery("select s from AmSystems s where s.sysState = 0 order by s.sysId", AmSystems.class).getResultList();
        Integer systemEvents = 0;
        Integer groupEvents = 0;

        for (AmSystems system : systemsList) {
            if (system.getSysState() == 0) {

                for (AmGroups group : system.getGroupsList()) {
                    group.setGrpStyle(null);

                    if (group.getGrpState() == 0) {

                        for (AmKpis kpi : group.getKpisList()) {
                            if (kpi.getKpiState() == 0) {
                                groupEvents += kpi.getKpiEventsCount();

                                if (kpi.getKpiEventsCount() > 0) {
                                    if (kpi.getKpiTypeId().equals(1)) {
                                        group.setGrpStyle("red");
                                    } else if (group.getGrpStyle() == null) {
                                        group.setGrpStyle("sandybrown");
                                    }
                                }
                            }
                        }


                        group.setGrpEventsCount(groupEvents);
                        em.merge(group);
                        systemEvents += groupEvents;
                        groupEvents = 0;
                    }
                }


                system.setSysEventsCount(systemEvents);
                em.merge(system);
                systemEvents = 0;
            }
        }

        return systemsList;
    }


    private boolean checkTime(AmKpis kpi) {
        if (kpi.getKpiLastRefreshTimestamp() == null) {
            return true;
        } else {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(kpi.getKpiLastRefreshTimestamp());
            calendar.add(Calendar.MINUTE, kpi.getKpiRefreshTimeMins());
            return calendar.getTime().before(new Date());
        }
    }

    public void updateKpai(AmKpis kpi) {
        em.merge(kpi);
    }

    public List<Object> getKpiResults(AmKpis kpi){
        return em.createNativeQuery(kpi.getKpiResultQuery()).getResultList();
    }

    public void refreshMonitor(){
        em.createQuery("update AmKpis s set s.kpiLastRefreshTimestamp = '01-01-2019'").executeUpdate();
    }


}
