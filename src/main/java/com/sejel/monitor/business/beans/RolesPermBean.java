package com.sejel.monitor.business.beans;

import com.sejel.common.ejb.AbstractFacade;
import com.sejel.monitor.persistence.entity.userManagement.*;
import com.sejel.monitor.presentation.helper.Utilitis.AppConstants;
import org.picketbox.commons.cipher.Base64;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.security.MessageDigest;
import java.util.Date;
import java.util.List;

@Stateless
public class RolesPermBean extends AbstractFacade<AmUserProfile>{
    @PersistenceContext(unitName = "MainUnit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RolesPermBean() {
            super(AmUserProfile.class);
    }

    public List<LuRoles> getAllRoles(){
        return em.createQuery("select s from LuRoles s where s.roleId != 1", LuRoles.class).getResultList();
    }

    public List<LuPermissions> getAllPerms(){
        return em.createQuery("select s from LuPermissions s where s.pId != 1", LuPermissions.class).getResultList();
    }

    public List<String> getUserPerms(String Username){
        return em.createQuery("select perm.pNameLa " +
                "from AmUserProfile u " +
                "inner join RoleUserMap rum on (u.uId = rum.rumUserId) " +
                "inner join RolePermMap rpm on (rum.rumRoleId = rpm.rpmRoleId) " +
                "inner join LuPermissions perm on (rpm.rpmPermId = perm.pId) " +
                "inner join LuRoles r on (r.roleId = rpm.rpmRoleId) " +
                "where u.uUsername = :user and u.uState = 0 and rum.rumState = 0 and rpm.rpmState = 0 and perm.pState = 0 and r.roleState = 0 " , String.class)
                .setParameter("user" , Username).getResultList();
    }


    public void changeRoleState(LuRoles role){
        if(role.getRoleState() == 0){
            role.setRoleState(512);
            em.merge(role);
        }else{
            role.setRoleState(0);
            em.merge(role);
        }
    }
    public void changePermState(LuPermissions perm){
        if(perm.getpState() == 0){
            perm.setpState(512);
            em.merge(perm);
        }else{
            perm.setpState(0);
            em.merge(perm);
        }
    }

    public void addRole(LuRoles role){

        if(role.getId()==null){
            role.setRoleId((Long) em.createNativeQuery(role.getSequenceIdQuery()).getSingleResult());
            role.setRoleState(0);
        }

        role.setCreateTimestamp(new Date());
        em.merge(role);

     }

    public boolean checkRoleNameAvailability(LuRoles role){
        return em.createQuery("select s from LuRoles s where (s.roleNameAr = :nameAr or s.roleNameLa = :nameLa) and s.roleId != :roleId",LuRoles.class)
                .setParameter("nameAr", role.getRoleNameAr())
                .setParameter("nameLa", role.getRoleNameLa())
                .setParameter("roleId", role.getRoleId() == null? 0:role.getRoleId())
                .getResultList().isEmpty();
    }

    public void addPermToRole(Long permId , Long role , String user){

        RolePermMap rpm = new RolePermMap();
        rpm.setRpmId((Long) em.createNativeQuery(rpm.getSequenceIdQuery()).getSingleResult());
        rpm.setRpmState(0);
        rpm.setRpmPermId(permId);
        rpm.setRpmRoleId(role);
        rpm.setChangeUsername(user);
        rpm.setChangeTimestamp(new Date());
        em.merge(rpm);
    }

    public void deletePermFromRole(Long permId , Long role){

        em.createQuery("delete from RolePermMap s where s.rpmRoleId = :role and s.rpmPermId = :perm")
                .setParameter("role" , role)
                .setParameter("perm" , permId)
                .executeUpdate();
    }

    public LuRoles reloadRole(Long roleId){
        try{
            return em.createQuery("select s from LuRoles s where s.roleId = :role",LuRoles.class).setParameter("role", roleId).getSingleResult();
        }catch (Exception e) {
            return new LuRoles();
        }
    }

    public AmUserProfile reloadUser(Long userId){
        try{
            return em.createQuery("select s from AmUserProfile s where s.uId = :user",AmUserProfile.class).setParameter("user", userId).getSingleResult();
        }catch (Exception e) {
            return new AmUserProfile();
        }
    }

    public void addPermToUser(Long permId , Long userId , String user){

        PermUserMap rpm = new PermUserMap();
        rpm.setPumId((Long) em.createNativeQuery(rpm.getSequenceIdQuery()).getSingleResult());
        rpm.setPumState(0);
        rpm.setPumPermId(permId);
        rpm.setPumUserId(userId);
        rpm.setChangeUsername(user);
        rpm.setChangeTimestamp(new Date());
        em.merge(rpm);
    }

    public void deletePermFromUser(Long permId , Long userId){

        em.createQuery("delete from PermUserMap s where s.pumUserId = :user and s.pumPermId = :perm")
                .setParameter("user" , userId)
                .setParameter("perm" , permId)
                .executeUpdate();
    }

    public void addRoleToUser(Long roleId , Long userId , String user){

        RoleUserMap rpm = new RoleUserMap();
        rpm.setRumId((Long) em.createNativeQuery(rpm.getSequenceIdQuery()).getSingleResult());
        rpm.setRumState(0);
        rpm.setRumRoleId(roleId);
        rpm.setRumUserId(userId);
        rpm.setChangeUsername(user);
        rpm.setChangeTimestamp(new Date());
        em.merge(rpm);
    }

    public void deleteRoleFromUser(Long roleId , Long userId){

        em.createQuery("delete from RoleUserMap s where s.rumUserId = :user and s.rumRoleId = :role")
                .setParameter("user" , userId)
                .setParameter("role" , roleId)
                .executeUpdate();
    }



}
