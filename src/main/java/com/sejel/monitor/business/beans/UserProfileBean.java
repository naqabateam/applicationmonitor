package com.sejel.monitor.business.beans;

import com.sejel.monitor.business.parentBeans.AbstractFacadeWrapper;
import com.sejel.monitor.persistence.entity.userManagement.AmUserProfile;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class UserProfileBean extends AbstractFacadeWrapper<AmUserProfile> {
    @PersistenceContext(unitName = "MainUnit")
    private EntityManager em;

    public UserProfileBean() {
        super(AmUserProfile.class);
    }

    public AmUserProfile getUserByUsername(String username) {
        return em.createQuery("select user from AmUserProfile user where lower(user.uUsername) = lower(:username) and user.uState = 0 ", AmUserProfile.class).setParameter("username", username).getSingleResult();
    }
}
