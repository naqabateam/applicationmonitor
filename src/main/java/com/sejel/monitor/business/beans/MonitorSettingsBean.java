package com.sejel.monitor.business.beans;

import com.sejel.monitor.business.parentBeans.AbstractFacadeWrapper;
import com.sejel.monitor.persistence.entity.AmGroups;
import com.sejel.monitor.persistence.entity.AmKpiType;
import com.sejel.monitor.persistence.entity.AmKpis;
import com.sejel.monitor.persistence.entity.AmSystems;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
public class MonitorSettingsBean extends AbstractFacadeWrapper<AmSystems> {
    @PersistenceContext(unitName = "MainUnit")
    private EntityManager em;

    public MonitorSettingsBean() {
        super(AmSystems.class);
    }

    public List<AmSystems> getAllAmSystems(){
        try{
            return em.createQuery("select s from AmSystems s", AmSystems.class).getResultList();
        }catch (Exception e){
            return new ArrayList<>();
        }
    }
    public List<AmGroups> getAllAmGroups(){
        try{
            return em.createQuery("select s from AmGroups s", AmGroups.class).getResultList();
        }catch (Exception e){
            return new ArrayList<>();
        }
    }
    public List<AmKpis> getAllAmKpis(){
        try{
            return em.createQuery("select s from AmKpis s", AmKpis.class).getResultList();
        }catch (Exception e){
            return new ArrayList<>();
        }
    }


    public void addSystem(AmSystems system){

        if(system.getSysId()==null){
            system.setSysId((Long) em.createNativeQuery(system.getSequenceIdQuery()).getSingleResult());
        }
        system.setCreateTimestamp(new Date());
        system.setSysEventsCount(0);
        system.setSysState(0);
        em.merge(system);
    }

    public void addGroup(AmGroups grp){

        if(grp.getGrpId()==null){
            grp.setGrpId((Long) em.createNativeQuery(grp.getSequenceIdQuery()).getSingleResult());
        }
        grp.setCreateTimestamp(new Date());
        grp.setGrpEventsCount(0);
        grp.setGrpState(0);
        em.merge(grp);
    }

    public void addKpi(AmKpis kpi){

        if(kpi.getKpiId()==null){
            kpi.setKpiId((Long) em.createNativeQuery(kpi.getSequenceIdQuery()).getSingleResult());
        }
        kpi.setKpiLastRefreshTimestamp(new Date());
        kpi.setCreateTimestamp(new Date());
        kpi.setKpiEventsCount(0);
        kpi.setKpiState(0);
        em.merge(kpi);
    }

    public void changeSystemState(AmSystems sys){
        if(sys.getSysState() == 0){
            sys.setSysState(512);
            em.merge(sys);
        }else{
            sys.setSysState(0);
            em.merge(sys);
        }
    }

    public void changeGroupState(AmGroups grp){
        if(grp.getGrpState() == 0){
            grp.setGrpState(512);
            em.merge(grp);
        }else{
            grp.setGrpState(0);
            em.merge(grp);
        }
    }

    public void changeKpiState(AmKpis kpi){
        if(kpi.getKpiState() == 0){
            kpi.setKpiState(512);
            em.merge(kpi);
        }else{
            kpi.setKpiState(0);
            em.merge(kpi);
        }
    }

    public List<AmKpiType> getKpiTypes(){
        return em.createQuery("select s from AmKpiType s",AmKpiType.class).getResultList();
    }

}
