package com.sejel.monitor.business.beans;

import com.sejel.common.ejb.AbstractFacade;
import com.sejel.monitor.persistence.entity.userManagement.AmUserProfile;
import com.sejel.monitor.persistence.entity.userManagement.LuRoles;
import com.sejel.monitor.persistence.entity.userManagement.PermUserMap;
import com.sejel.monitor.persistence.entity.userManagement.RoleUserMap;
import com.sejel.monitor.presentation.helper.Utilitis.AppConstants;
import org.picketbox.commons.cipher.Base64;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.security.MessageDigest;
import java.util.Date;
import java.util.List;

@Stateless
public class UserManagementBean extends AbstractFacade<AmUserProfile>{
    @PersistenceContext(unitName = "MainUnit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserManagementBean() {
            super(AmUserProfile.class);
    }

    public List<AmUserProfile> getAllUsersWithoutCurrentUser(String currentUser){
        return em.createQuery("select s from AmUserProfile s where s.uUsername != :user", AmUserProfile.class).setParameter("user",currentUser).getResultList();
    }


    public boolean checkUsernameAvailability(AmUserProfile user){
        return em.createQuery("select s from AmUserProfile s where s.uUsername = :username and s.uId != :currentUser",AmUserProfile.class)
                .setParameter("username", user.getuUsername())
                .setParameter("currentUser", user.getuId() == null? 0:user.getuId())
                .getResultList().isEmpty();
    }

    public void changeSystemState(AmUserProfile user){
        if(user.getuState() == 0){
            user.setuState(512);
            em.merge(user);
        }else{
            user.setuState(0);
            em.merge(user);
        }
    }

    public void addUser(AmUserProfile user){

        if(user.getId()==null){
            user.setuId((Long) em.createNativeQuery(user.getSequenceIdQuery()).getSingleResult());
            user.setuPassword(getMD5Hash("123"));
            user.setuState(0);

            PermUserMap permMap = new PermUserMap();
            permMap.setPumId((Long) em.createNativeQuery(permMap.getSequenceIdQuery()).getSingleResult());
            permMap.setPumPermId(AppConstants.PERM_WEB_PAGES);
            permMap.setPumUserId(user.getuId());
            permMap.setPumState(0);
            permMap.setCreateTimestamp(new Date());
            permMap.setChangeUsername(user.getChangeUsername());
            em.merge(permMap);
        }

        user.setCreateTimestamp(new Date());
        em.merge(user);

    }

    private static String getMD5Hash(String pass) {
        try {
            byte[] bytesOfMessage = pass.getBytes("UTF-8");

            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] thedigest = md.digest(bytesOfMessage);

            return Base64.encodeBytes(thedigest);

        } catch (Exception ignored) {

        }
        return "";
    }
}
