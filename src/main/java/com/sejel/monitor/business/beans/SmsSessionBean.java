package com.sejel.monitor.business.beans;

import com.sejel.common.ejb.AbstractFacade;
import com.sejel.monitor.persistence.entity.LuSmsMsgs;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;

@Stateless
public class SmsSessionBean extends AbstractFacade<LuSmsMsgs> {
    @PersistenceContext(unitName = "MainUnit")
    private EntityManager em;

    public SmsSessionBean() {
        super(LuSmsMsgs.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void saveSms(long countryCode, long mobileNo, String msg, String username) {
        LuSmsMsgs sms = new LuSmsMsgs();
        sms.setSmsSender("TIC");
        sms.setSmsReceivers(countryCode + "" + mobileNo);
        sms.setSmsTimestampLa(new Date());
        sms.setSmsProcessingStatus("N");
        //sms.setSmsProcessingTimestampLa(new Date());
        sms.setSmsUsername(username);
        sms.setSmsBody(msg);
        add(sms);
    }
}
