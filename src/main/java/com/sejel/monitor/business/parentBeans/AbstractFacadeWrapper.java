package com.sejel.monitor.business.parentBeans;

import com.sejel.common.ejb.AbstractFacade;
import com.sejel.common.jpa.JPAAbstractRoot;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class AbstractFacadeWrapper<T extends JPAAbstractRoot> extends AbstractFacade<T> {
    @PersistenceContext(unitName = "MainUnit")
    private EntityManager em;

    public AbstractFacadeWrapper(Class<T> aClass) {
        super(aClass);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    public Object updateOrSaveAndReturnObject(T object) {
        getEntityManager().merge(object);
        getEntityManager().flush();
        return object;

    }

}
