package com.sejel.monitor.business.customeExceptions;


import com.sejel.monitor.business.helper.Result;

public class BusinessException extends Exception {

    protected Long errorCode;

    protected String errorDesc;

    protected String[] params;

    protected String errorLocaleMsg;

    protected String errorDescAr ,errorDescLa;

    public BusinessException(Result error) {
        this.errorCode=error.getErrorCode().longValue();
        this.errorDescAr =error.getErrorDescriptionAr();
        this.errorDescLa =error.getErrorDescriptionLa();
    }

    public BusinessException(String message, String errorLocaleMsg) {
        super(message);
        this.errorLocaleMsg = errorLocaleMsg;
    }

    public BusinessException(String message, String... params) {
        super(message);
        this.params = params;
    }

    public BusinessException(String message, Throwable cause, String... params) {
        super(message, cause);
        this.params = params;
    }

    public BusinessException(String message, Long errorCode, String errorDesc,
                            String... params) {
        super(message);
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
        this.params = params;
    }

    public BusinessException(String message, Long errorCode, String errorDesc, Throwable cause,
                            String... params) {
        super(message, cause);
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
        this.params = params;
    }

    public BusinessException(Throwable cause) {
        super(cause);
        this.errorCode = errorCode;
        this.params = params;
    }

    public BusinessException(String message, Long errorCode, String errorDesc, Throwable cause,
                            boolean enableSuppression, boolean writableStackTrace, String... params) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.errorCode = errorCode;
        this.params = params;
    }


    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    public Long getErrorCode() {
        return errorCode;
    }


    public void setErrorCode(Long errorCode) {
        this.errorCode = errorCode;
    }

    public String[] getParams() {
        return params;
    }

    public void setParams(String[] params) {
        this.params = params;
    }

    public String getErrorLocaleMsg() {
        return errorLocaleMsg;
    }

    public void setErrorLocaleMsg(String errorLocaleMsg) {
        this.errorLocaleMsg = errorLocaleMsg;
    }

    public String getErrorDescAr() {
        return errorDescAr;
    }

    public void setErrorDescAr(String errorDescAr) {
        this.errorDescAr = errorDescAr;
    }

    public String getErrorDescLa() {
        return errorDescLa;
    }

    public void setErrorDescLa(String errorDescla) {
        this.errorDescLa = errorDescla;
    }
}