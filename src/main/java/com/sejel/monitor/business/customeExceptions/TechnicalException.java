package com.sejel.monitor.business.customeExceptions;

public class TechnicalException extends Exception {

  String errMsgKey;

  String message;

  String errorLocaleMsg;

  public TechnicalException(String message, String errorLocaleMsg) {
    super(message);
    this.errorLocaleMsg = errorLocaleMsg;
  }

  public TechnicalException(String message, Throwable cause, String errMsgKey) {
    super(message, cause);
    this.errMsgKey = errMsgKey;
  }

  public TechnicalException(String message, Throwable cause) {
    super(message, cause);
  }

  public TechnicalException(String message) {
    super(message);
    this.message = message;
  }

  public String getErrMsgKey() {
    return errMsgKey;
  }

  public void setErrMsgKey(String errMsgKey) {
    this.errMsgKey = errMsgKey;
  }

  @Override
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
