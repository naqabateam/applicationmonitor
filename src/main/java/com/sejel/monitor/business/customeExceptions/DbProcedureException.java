package com.sejel.monitor.business.customeExceptions;


import java.util.Arrays;

public class DbProcedureException extends BusinessException {

    protected String procedureName;

    public DbProcedureException(Long errorCode, String errorDesc, String procedureName,
                                String... params) {
        super(errorCode.toString(), errorCode, errorDesc, params);
        this.procedureName = procedureName;
    }

    public DbProcedureException(Long errorCode) {
        super(errorCode.toString());
    }
    public DbProcedureException( String msg) {
       super(msg);
    }
    public DbProcedureException(Long errorCode,String msg) {
        super(errorCode.toString(),msg);
    }

    public DbProcedureException(Long errorCode, String errorDesc, Throwable cause,
                                String procedureName, String... params) {
        super(errorCode.toString(), errorCode, errorDesc, cause, params);
        this.procedureName = procedureName;
    }

    public DbProcedureException(Throwable cause, String procedureName) {
        super(cause);
        this.procedureName = procedureName;
    }

    public DbProcedureException(Long errorCode, String errorDesc, Throwable cause,
                                boolean enableSuppression, boolean writableStackTrace, String procedureName,
                                String... params) {
        super(errorCode.toString(), errorCode, errorDesc, cause, enableSuppression, writableStackTrace,
                params);
        this.procedureName = procedureName;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    @Override
    public String toString() {
        return "DbProcedureException{" +
                "errorCode='" + errorCode + '\'' +
                "errorDesc='" + errorDesc + '\'' +
                "params='" + Arrays.asList(params) + '\'' +
                "procedureName='" + procedureName + '\'' +
                '}';
    }
}