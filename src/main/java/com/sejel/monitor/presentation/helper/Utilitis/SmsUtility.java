package com.sejel.monitor.presentation.helper.Utilitis;

import com.sejel.monitor.business.beans.SmsSessionBean;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.text.MessageFormat;
import java.util.ResourceBundle;

@ApplicationScoped
@Named
public class SmsUtility {
    @Inject private SmsSessionBean smsFacade;

    public void sendSms(long countryCode, long mobileNo, String username, String msgKey, String... params) {
        FacesContext context = FacesContext.getCurrentInstance();
        ResourceBundle bundle = ResourceBundle.getBundle("localeResources", context.getViewRoot().getLocale());
        MessageFormat mf = new MessageFormat(bundle.getString(msgKey));
        String body = mf.format(params);
        smsFacade.saveSms(countryCode, mobileNo, body, username);
    }
}
