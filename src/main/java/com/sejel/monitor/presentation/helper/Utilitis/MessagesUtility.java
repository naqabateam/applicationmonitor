package com.sejel.monitor.presentation.helper.Utilitis;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

@Named
public class MessagesUtility implements Serializable {
    public static enum MessageType {
        SUCCESS(1), ERROR(2), VALIDATION(3);
        int type;

        MessageType(int type) {
            this.type = type;
        }
    }

    public void addSuccessMessageToPage(String clientId, String msgKey, String summaryKey) {
        try {
            ResourceBundle labels = getLocale("localeResources");
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_INFO, labels.getString(summaryKey), labels.getString(msgKey)));
            context.getExternalContext().getFlash().setKeepMessages(true);
        } catch (Exception e) {

        }
    }

    public void addErrorMessageToPage(String clientId, String msgKey, String summaryKey) {
        try {
            ResourceBundle labels = getLocale("localeResources");
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_ERROR, labels.getString(summaryKey), labels.getString(msgKey)));
            context.getExternalContext().getFlash().setKeepMessages(true);
        } catch (Exception e) {

        }
    }

    public void addErrorMessageToPage(String clientId, String msg) {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
            context.getExternalContext().getFlash().setKeepMessages(true);
        } catch (Exception e) {

        }
    }

    public void addErrorMessageToPage(String clientId, String msgKey, String summaryKey, Object... params) {
        try {
            ResourceBundle labels = getLocale("localeResources");
            FacesContext context = FacesContext.getCurrentInstance();
            String summaryMsg = MessageFormat.format(labels.getString(summaryKey), params);
            String msg = MessageFormat.format(labels.getString(msgKey), params);
            context.addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_ERROR, summaryMsg, msg));
            context.getExternalContext().getFlash().setKeepMessages(true);
        } catch (Exception e) {

        }
    }

    public void addValidationMessageToPage(String clientId, String msgKey, String summaryKey) {
        ResourceBundle labels = getLocale("ValidationMessages");
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_ERROR, labels.getString(summaryKey), labels.getString(msgKey)));
        context.getExternalContext().getFlash().setKeepMessages(true);

    }

    private ResourceBundle getLocale(String localName) {
        FacesContext context = FacesContext.getCurrentInstance();
        ResourceBundle bundle = ResourceBundle
                .getBundle(localName, context.getViewRoot().getLocale());
        return bundle;
    }

    public String getLocaleOfKeyOnly(String key) {
        try {
            ResourceBundle labels = getLocale("localeResources");
            return labels.getString(key);
        } catch (Exception e) {

        }
        return key;
    }

    public String getValueByKey(String language, String key) {
        FacesContext context = FacesContext.getCurrentInstance();
        Locale locale = new Locale(language);
        ResourceBundle bundle = ResourceBundle
                .getBundle("localeResources", locale);
        String value = bundle.getString(key);
        return value;
    }

    public void addMessageWithParams(String clientId, String msgKey, String summaryKey, MessageType messagetype, Object... params) {

        ResourceBundle labels = getLocale("ValidationMessages");
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage.Severity severity = null;
        switch (messagetype) {
            case SUCCESS:
                severity = FacesMessage.SEVERITY_INFO;
                break;
            case ERROR:
            case VALIDATION:
                severity = FacesMessage.SEVERITY_ERROR;
                break;


        }
        String message = MessageFormat.format(getLocaleOfKeyOnly(msgKey), params);
        context.addMessage(clientId, new FacesMessage(severity, message, message));
        context.getExternalContext().getFlash().setKeepMessages(true);


    }


}
