package com.sejel.monitor.presentation.helper.Utilitis;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtility {


    // take full date formate  As string and convert it as basic date  in string formatt
    public static String fullDateTimeToDateOnly(String fullDateAsString) throws ParseException {

        DateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
        Date date = format.parse(fullDateAsString);
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = dateFormat.format(date);
        return strDate;

    }
}
