/**
 * Class description
 */

/*
 * ==================================================================================================
 * ======= Ver No:| Created/Updated Date | Created/Updated By | Comments
 * ============================
 * ============================================================================= 1 | 18/Nov/2010 |
 * Author | InItial Version
 * ==========================================================================
 * =================================
 */

package com.sejel.monitor.presentation.helper.Utilitis;

public class HijriDateFormatter{



    public static String getFormattedHijriDate(String value) {
        if (value != null) {
            if (!value.trim().equals("") && value.length() >= 8) {

                return value.substring(6, 8) + "/" + value.substring(4, 6) + "/" + value.substring(0, 4);
            }
        }
        return "";
    }
}
