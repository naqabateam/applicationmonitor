package com.sejel.monitor.presentation.helper.Utilitis;

public class JSFGeneratorParam implements Comparable<JSFGeneratorParam> {

    private String property;

    private String dataType;

    private boolean required;

    private int order;

    private boolean dataTypeEditable;

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public boolean isDataTypeEditable() {
        return dataTypeEditable;
    }

    public void setDataTypeEditable(boolean dataTypeEditable) {
        this.dataTypeEditable = dataTypeEditable;
    }

    @Override
    public int compareTo(JSFGeneratorParam paramT) {

        if (this.order == 0) {
            return -1;
        }

        int compareOrder = ((JSFGeneratorParam) paramT).getOrder();

        //ascending order
        return this.order - compareOrder;

        //descending order
        //return compareOrder - this.order;

    }

}
