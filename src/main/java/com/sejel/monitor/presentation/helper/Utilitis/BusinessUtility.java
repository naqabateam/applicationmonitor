package com.sejel.monitor.presentation.helper.Utilitis;

import org.picketbox.commons.cipher.Base64;

import java.security.MessageDigest;
import java.util.Random;

public class BusinessUtility {

    public static Long getParticipantYearsForBus(Long joinedSeasonYear) {
        Long currentSeason = 1440L;
        return currentSeason - joinedSeasonYear;
    }


    public static String generateRandomChar() {
        Random r = new Random();

        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 3; i++) {
            sb.append(alphabet.charAt(r.nextInt(alphabet.length())));
        }
        for (int i = 0; i < 3; i++) {
            sb.append(r.nextInt(9));
        }
        return sb.toString();
    }

    public static String getMD5Hash(String message) {
        try {
            byte[] bytesOfMessage = message.getBytes("UTF-8");

            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] thedigest = md.digest(bytesOfMessage);

            String hash = Base64.encodeBytes(thedigest);
            return hash;

        } catch (Exception e) {

        }
        return "";
    }


//    private LatLng findNearestPoint(LatLng test, List<LatLng> target) {
//        Location closestLocation;
//        int smallestDistance = -1;
//        for(Location location : locations){
//            int distance = Location.distanceBetween(closestLocation.getLatitude(),
//                    closestLocation.getLongitude(),
//                    location.getLatitude(),
//                    location.getLongitude());
//            if(smallestDistance == -1 || distance < smallestDistance){
//                closestLocation = location;
//                smallestDistance = distance;
//            }
//        }
//
//    }



    public static double distance(double lat1, double lon1, double lat2, double lon2) {
        String unit = "K";
        if ((lat1 == lat2) && (lon1 == lon2)) {
            return 0;
        } else {
            double theta = lon1 - lon2;
            double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2)) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            dist = dist * 60 * 1.1515;
            if (unit == "K") {
                dist = dist * 1.609344;
            } else if (unit == "N") {
                dist = dist * 0.8684;
            }
            return (dist);
        }
    }

}
