package com.sejel.monitor.presentation.helper.Utilitis;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@Named
@ApplicationScoped
public class AppConstants {

    public static final Long PERM_WEB_PAGES = 1L;

}