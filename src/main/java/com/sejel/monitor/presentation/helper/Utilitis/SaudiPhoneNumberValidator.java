package com.sejel.monitor.presentation.helper.Utilitis;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("SaudiNumberValidation")
public class SaudiPhoneNumberValidator implements Validator {
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (context != null && component != null) {
            if (value != null) {
                String objString;
                String regexPattern = "^[1-9]\\d*";
                FacesMessage errorMsg =
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "system.required.number", "system.required.number");
                try {
                    objString =  value.toString();
                } catch (Exception ex) {
                    throw new ValidatorException(errorMsg);
                }

                if (!objString.matches(regexPattern)|| !objString.startsWith("5") || objString.length()!=9) {
                    throw new ValidatorException(errorMsg);
                }
            }
        }
    }
}
