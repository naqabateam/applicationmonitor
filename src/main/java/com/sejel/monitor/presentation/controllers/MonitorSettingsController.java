package com.sejel.monitor.presentation.controllers;

import com.sejel.common.util.FilterColumn;
import com.sejel.common.util.SortColumn;
import com.sejel.monitor.business.beans.MonitorSettingsBean;
import com.sejel.monitor.persistence.entity.AmGroups;
import com.sejel.monitor.persistence.entity.AmKpiType;
import com.sejel.monitor.persistence.entity.AmKpis;
import com.sejel.monitor.persistence.entity.AmSystems;
import com.sejel.monitor.presentation.controllers.UserManagement.LoginController;
import com.sejel.monitor.presentation.controllers.common.AbstractControllerWrapper;
import com.sejel.monitor.presentation.controllers.common.AbstractPrimefacesModelWrapper;
import com.sejel.monitor.presentation.helper.Utilitis.MessagesUtility;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Named
@ViewAccessScoped
public class MonitorSettingsController extends AbstractControllerWrapper<AmSystems> implements Serializable {

    @Inject
    private MonitorSettingsBean entityBean;
    @Inject
    private MessagesUtility messagesUtility;
    @Inject
    private LoginController loginController;

    private List<AmSystems> systemsList = new ArrayList<>();
    private List<AmGroups> groupsList = new ArrayList<>();
    private List<AmKpis> kpisList = new ArrayList<>();

    private AmSystems newSystem;
    private AmGroups newGroup;
    private AmKpis newKpi;

    private List<AmKpiType> kpiTypes = new ArrayList<>();

    private String currentUser;


    @PostConstruct
    public void init() {
        currentUser = loginController.getUserProfile().getuUsername();
        kpiTypes = entityBean.getKpiTypes();
        clearFields();
        reloadSystems();
        reloadGroups();
        reloadKpis();
    }

    private void clearFields(){
        newSystem = new AmSystems();
        newGroup = new AmGroups();
        newKpi = new AmKpis();
    }

    private void reloadSystems(){
        systemsList = entityBean.getAllAmSystems();
    }
    private void reloadGroups(){
        groupsList = entityBean.getAllAmGroups();
    }
    private void reloadKpis(){
        kpisList = entityBean.getAllAmKpis();
    }

    public String addSystem(){
        try {
            if(newSystem.getSysName() != null) {
                newSystem.setChangeUsername(this.currentUser);
                entityBean.addSystem(newSystem);
                messagesUtility.addSuccessMessageToPage(null, "success.message", "success.message");
            }else{
                messagesUtility.addErrorMessageToPage(null , "add.system.validation.sys.name" ,"add.system.validation.sys.name");
                return "";
            }
            clearFields();
            reloadSystems();
            return "/pages/Settings/SystemSettings.xhtml?faces-redirect=true";
        }catch (Exception e){
            messagesUtility.addErrorMessageToPage(null , "system.error.message" ,"system.error.message");
            return "";
        }
    }

    public String addGroup(){
        try {
            if(validateGroup()) {
                newGroup.setChangeUsername(this.currentUser);
                entityBean.addGroup(newGroup);
                messagesUtility.addSuccessMessageToPage(null, "success.message", "success.message");
            }else{
                return "";
            }
            clearFields();
            reloadGroups();
            return "/pages/Settings/GroupsSettings.xhtml?faces-redirect=true";
        }catch (Exception e){
            messagesUtility.addErrorMessageToPage(null , "system.error.message" ,"system.error.message");
            return "";
        }
    }

    private boolean validateGroup(){
        if(newGroup.getGrpName() == null){
            messagesUtility.addErrorMessageToPage(null , "add.group.validation.grp.name" ,"add.group.validation.grp.name");
            return false;
        }
        if(newGroup.getGrpSysId() == null){
            messagesUtility.addErrorMessageToPage(null , "add.group.validation.grp.sys.id" ,"add.group.validation.grp.sys.id");
            return false;
        }

        return true;
    }

    public String addKpi(){
        try {
            if(validateKpi()) {
                newKpi.setChangeUsername(this.currentUser);
                entityBean.addKpi(newKpi);
                messagesUtility.addSuccessMessageToPage(null, "success.message", "success.message");
            }
            else{
                return "";
            }
            clearFields();
            reloadKpis();
            return "/pages/Settings/KpisSettings.xhtml?faces-redirect=true";
        }catch (Exception e){
            messagesUtility.addErrorMessageToPage(null , "system.error.message" ,"system.error.message");
            return "";
        }
    }

    private boolean validateKpi(){
        if(newKpi.getKpiName() == null){
            messagesUtility.addErrorMessageToPage(null , "add.kpi.validation.kpi.name" ,"add.kpi.validation.kpi.name");
            return false;
        }
        if(newKpi.getKpiGrpId() == null){
            messagesUtility.addErrorMessageToPage(null , "add.kpi.validation.kpi.grp.id" ,"add.kpi.validation.kpi.grp.id");
            return false;
        }
        if(newKpi.getKpiCheckQuery() == null){
            messagesUtility.addErrorMessageToPage(null , "add.kpi.validation.check.query" ,"add.kpi.validation.check.query");
            return false;
        }
        if(newKpi.getKpiResultQuery() == null){
            messagesUtility.addErrorMessageToPage(null , "add.kpi.validation.result.query" ,"add.kpi.validation.result.query");
            return false;
        }
        if(newKpi.getKpiTypeId() == null){
            messagesUtility.addErrorMessageToPage(null , "add.kpi.validation.kpi.type" ,"add.kpi.validation.kpi.type");
            return false;
        }

        return true;
    }

    public String cancel(String page){
        init();
        return "/pages/Settings/"+page+".xhtml?faces-redirect=true";
    }


    public String changeSystemState(AmSystems sys){
        entityBean.changeSystemState(sys);
        messagesUtility.addSuccessMessageToPage(null, "success.message", "success.message");
        clearFields();
        reloadSystems();
        return "/pages/Settings/SystemSettings.xhtml?faces-redirect=true";
    }
    public String editSystem(AmSystems sys){
        newSystem = sys;
        return "/pages/Settings/SystemSettings.xhtml?faces-redirect=true";
    }

    public String changeGroupState(AmGroups grp){
        entityBean.changeGroupState(grp);
        messagesUtility.addSuccessMessageToPage(null, "success.message", "success.message");
        clearFields();
        reloadGroups();
        return "/pages/Settings/GroupsSettings.xhtml?faces-redirect=true";
    }
    public String editGroup(AmGroups grp){
        newGroup = grp;
        return "/pages/Settings/GroupsSettings.xhtml?faces-redirect=true";
    }

    public String changeKpiState(AmKpis kpi){
        entityBean.changeKpiState(kpi);
        messagesUtility.addSuccessMessageToPage(null, "success.message", "success.message");
        clearFields();
        reloadGroups();
        return "/pages/Settings/KpisSettings.xhtml?faces-redirect=true";
    }
    public String editKpi(AmKpis kpi){
        newKpi = kpi;
        return "/pages/Settings/KpisSettings.xhtml?faces-redirect=true";
    }

    private static class LuTempDataModel extends AbstractPrimefacesModelWrapper<AmSystems> {

        private MonitorSettingsController cntrl;

        LuTempDataModel(MonitorSettingsController cntrl) {
            this.cntrl = cntrl;
        }

        @Override
        protected Object getId(AmSystems driverOutsourceTemp) {
            return driverOutsourceTemp.getId();
        }

        @Override
        protected int getFilteredCount(List<FilterColumn> filterFields) {
            return cntrl.getAllRowsCount(filterFields);
        }

        @Override
        protected List<AmSystems> getSortedFilteredList(int[] range, List<SortColumn> sortFields, List<FilterColumn> filterFields) {
            return cntrl.getList(range, sortFields, filterFields);
        }

        protected Map<String, String> getFilterOperation() {
            return cntrl.filterOperations;
        }

        @Override
        protected Map<String, Object> getFilterValues() {
            return cntrl.filterValues;
        }

    }

    @Override
    public void loadListItems() {
        primeItems = new LuTempDataModel(this);
    }


    @Override
    protected List<AmSystems> getList(int[] range, List<SortColumn> sortFields, List<FilterColumn> filterFields) {
        return entityBean.findSortedfilteredRange(range, sortFields, filterFields);
    }

    @Override
    protected int getAllRowsCount(List<FilterColumn> filterFields) {
        return entityBean.findFilteredListCount(filterFields);
    }

    @Override
    public String prepareAdd() {
        return "";
    }


    @Override
    public String prepareEditDetails(AmSystems editRec) {
        return null;
    }

    @Override
    public String update() {
        return null;
    }

    @Override
    public String prepareViewDetails(AmSystems viewRec) {
        current = viewRec;
        return "View.xhtml?faces-redirect=true";

    }

    @Override
    public String prepareList() {
        return "";
    }

    @Override
    public String addNew() {
            return "";
    }


    @Override
    public String activate() {
        return null;
    }

    @Override
    public String deactivate() {
        return null;
    }

    @Override
    public String prepareDeActivate() {
        return null;
    }

    @Override
    public String prepareActivate() {
        return null;
    }

    public List<AmSystems> getSystemsList() {
        return systemsList;
    }

    public void setSystemsList(List<AmSystems> systemsList) {
        this.systemsList = systemsList;
    }

    public List<AmGroups> getGroupsList() {
        return groupsList;
    }

    public void setGroupsList(List<AmGroups> groupsList) {
        this.groupsList = groupsList;
    }

    public List<AmKpis> getKpisList() {
        return kpisList;
    }

    public void setKpisList(List<AmKpis> kpisList) {
        this.kpisList = kpisList;
    }

    public AmSystems getNewSystem() {
        return newSystem;
    }

    public void setNewSystem(AmSystems newSystem) {
        this.newSystem = newSystem;
    }

    public AmGroups getNewGroup() {
        return newGroup;
    }

    public void setNewGroup(AmGroups newGroup) {
        this.newGroup = newGroup;
    }

    public AmKpis getNewKpi() {
        return newKpi;
    }

    public void setNewKpi(AmKpis newKpi) {
        this.newKpi = newKpi;
    }

    public List<AmKpiType> getKpiTypes() {
        return kpiTypes;
    }

    public void setKpiTypes(List<AmKpiType> kpiTypes) {
        this.kpiTypes = kpiTypes;
    }
}