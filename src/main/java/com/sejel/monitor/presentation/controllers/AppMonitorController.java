package com.sejel.monitor.presentation.controllers;

import com.sejel.monitor.business.beans.ApplicationMonitorBean;
import com.sejel.monitor.persistence.entity.AmGroups;
import com.sejel.monitor.persistence.entity.AmKpis;
import com.sejel.monitor.persistence.entity.AmSystems;
import com.sejel.monitor.presentation.controllers.UserManagement.LoginController;
import com.sejel.monitor.presentation.helper.Utilitis.MessagesUtility;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Named
@ViewAccessScoped
public class AppMonitorController implements Serializable {

    @Inject
    private ApplicationMonitorBean entityBean;
    @Inject
    private MessagesUtility messagesUtility;
    @Inject
    private LoginController loginController;
    @Inject
    private KpiResultsController resultsController;

    private List<AmSystems> systemsList = new ArrayList<>();
    private List<AmKpis> kpisList = new ArrayList<>();
    private AmGroups currentGroup;
    private AmKpis currentKpi;

    private Long currentUser;

    @PostConstruct
    public void init() {
        currentUser = loginController.getUserProfile().getuId();
    }

    public String backToKpiList(){
        return "/pages/Monitoring/Kpis.xhtml?faces-redirect=true";
    }
    public String backToDashBoard(){
        return "/pages/Monitoring/DashBoard.xhtml?faces-redirect=true";
    }

    public void reloadMonitorPage() throws IOException {
        refreshDashboard();
        FacesContext.getCurrentInstance().getExternalContext().redirect("DashBoard.xhtml");
        FacesContext.getCurrentInstance().responseComplete();

    }

    public void reloadKpiPage() throws IOException {
        refreshDashboard();
        FacesContext.getCurrentInstance().getExternalContext().redirect("Kpis.xhtml");
        FacesContext.getCurrentInstance().responseComplete();
    }


    public void refreshDashboard(){
        systemsList = entityBean.refreshDashBoard();
        if(currentGroup!=null) {
            kpisList = entityBean.getKpisForGroup(currentGroup.getGrpId());
        }
    }

    public void checkGroupEvents(AmGroups grp) throws IOException {
        currentGroup = grp;
        kpisList = entityBean.getKpisForGroup(grp.getGrpId());
        FacesContext.getCurrentInstance().getExternalContext().redirect("Kpis.xhtml");
        FacesContext.getCurrentInstance().responseComplete();
    }

    public String checkKpiDetails(AmKpis kpi){
        prepareEvents(kpi);
        return "Events.xhtml?faces-redirect=true";
    }

    public String assignKpi(AmKpis kpi){
        if(kpi.getUserProfile() == null){
            kpi.setEventUserId(currentUser);
            entityBean.updateKpai(kpi);

        }else if(kpi.getEventUserId().equals(currentUser)){
            kpi.setEventUserId(null);
            entityBean.updateKpai(kpi);
        }
        return "";
    }


    public boolean checkCanUnassign(AmKpis kpi){
        return kpi.getEventUserId().equals(currentUser);
    }





    private List<String> coulmnsHeaders = new ArrayList<>();

    private List<KpiResultsController.ColumnModel> columns;
    private List<Object> resaults;

    public void prepareEvents(AmKpis kpi) {
        resaults = new ArrayList<>();
        resaults = entityBean.getKpiResults(kpi);

        coulmnsHeaders = new ArrayList<>();
        coulmnsHeaders = Arrays.asList(kpi.getColumnsTemplate().split(","));

        createDynamicColumns();
    }

    static public class ColumnModel implements Serializable {

        private String header;
        private String property;

        ColumnModel(String header, String property) {
            this.header = header;
            this.property = property;
        }

        public String getHeader() {
            return header;
        }

        public String getProperty() {
            return property;
        }
    }


    private void createDynamicColumns() {

        columns = new ArrayList<KpiResultsController.ColumnModel>();

        for(int i = 0 ; i<coulmnsHeaders.size(); i++){
            columns.add(new KpiResultsController.ColumnModel(coulmnsHeaders.get(i), String.valueOf(i)));
        }
    }


    public void refreshMonitor(){
        entityBean.refreshMonitor();
    }



    public ApplicationMonitorBean getEntityBean() {
        return entityBean;
    }

    public void setEntityBean(ApplicationMonitorBean entityBean) {
        this.entityBean = entityBean;
    }

    public List<String> getCoulmnsHeaders() {
        return coulmnsHeaders;
    }

    public void setCoulmnsHeaders(List<String> coulmnsHeaders) {
        this.coulmnsHeaders = coulmnsHeaders;
    }

    public List<KpiResultsController.ColumnModel> getColumns() {
        return columns;
    }

    public void setColumns(List<KpiResultsController.ColumnModel> columns) {
        this.columns = columns;
    }

    public List<Object> getResaults() {
        return resaults;
    }

    public void setResaults(List<Object> resaults) {
        this.resaults = resaults;
    }

    public List<AmSystems> getSystemsList() {
        return systemsList;
    }

    public void setSystemsList(List<AmSystems> systemsList) {
        this.systemsList = systemsList;
    }

    public AmGroups getCurrentGroup() {
        return currentGroup;
    }

    public void setCurrentGroup(AmGroups currentGroup) {
        this.currentGroup = currentGroup;
    }

    public AmKpis getCurrentKpi() {
        return currentKpi;
    }

    public void setCurrentKpi(AmKpis currentKpi) {
        this.currentKpi = currentKpi;
    }

    public Long getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(Long currentUser) {
        this.currentUser = currentUser;
    }

    public List<AmKpis> getKpisList() {
        return kpisList;
    }

    public void setKpisList(List<AmKpis> kpisList) {
        this.kpisList = kpisList;
    }
}