package com.sejel.monitor.presentation.controllers.UserManagement;

import com.sejel.monitor.business.beans.UserProfileBean;
import com.sejel.monitor.persistence.entity.userManagement.AmUserProfile;
import com.sejel.monitor.presentation.helper.Utilitis.BusinessUtility;
import com.sejel.monitor.presentation.helper.Utilitis.MessagesUtility;
import org.picketbox.commons.cipher.Base64;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.security.MessageDigest;

@Named
@SessionScoped
public class LoginController implements Serializable {
    @Inject
    private UserProfileBean userProfileBean;
    @Inject
    private MessagesUtility messagesUtility;

    private AmUserProfile userProfile;

    private String username;
    private String password;
    private Long partyTypeId, partyEntityId, repId, committeeId;
    private boolean partyAdmin = false;
    private String newPassword, confirmPassword;
    private boolean committeeUser = false;


    public String login() {
        try {
            getRequest().logout();

            userProfile = userProfileBean.getUserByUsername(username.trim());

            getRequest().login(username.trim(), password.trim());
            password = null;

            return "/pages/DashBoard.xhtml?faces-redirect=true";

        } catch (Exception e) {
            System.out.println(e.getMessage());
            messagesUtility.addErrorMessageToPage(null, "login.failed", "login.failed");
            return "";
        }

    }

    public String prepareChangePassword() {
        newPassword = null;
        confirmPassword = null;
        password = null;
        return "/ChangePassword?faces-redirect=true";
    }

    public String saveNewPassword() {
        if(!getMD5Hash(password).equals(userProfile.getuPassword())) {
            messagesUtility.addErrorMessageToPage(null, "oldpass.wrong", "oldpass.wrong");
            return "";
        }

        if(!newPassword.equals(confirmPassword)) {
            messagesUtility.addErrorMessageToPage(null, "notmach.error", "notmach.error");
            return "";
        }

        String passTemp = getMD5Hash(newPassword);
        userProfile.setuPassword(passTemp);
        userProfileBean.edit(userProfile);
        messagesUtility.addSuccessMessageToPage(null, "password.changed.msg", "password.changed.msg");
        return "/pages/DashBoard.xhtml?faces-redirect=true";
    }

    private static String getMD5Hash(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] thedigest = md.digest(input.getBytes("UTF-8"));
            return Base64.encodeBytes(thedigest);

        } catch (Exception ignore) {
        }

        return "";
    }

    public String logout() {
        try {
            getRequest().getSession().invalidate();
            getRequest().logout();
            return "/login.xhtml?faces-redirect=true";
        } catch (ServletException e) {
            return "/login.xhtml?faces-redirect=true";
        }

    }

    public HttpServletRequest getRequest() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
        return request;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public AmUserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(AmUserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public Long getPartyTypeId() {
        return partyTypeId;
    }

    public void setPartyTypeId(Long partyTypeId) {
        this.partyTypeId = partyTypeId;
    }

    public Long getPartyEntityId() {
        return partyEntityId;
    }

    public void setPartyEntityId(Long partyEntityId) {
        this.partyEntityId = partyEntityId;
    }

    public boolean isPartyAdmin() {
        return partyAdmin;
    }

    public void setPartyAdmin(boolean partyAdmin) {
        this.partyAdmin = partyAdmin;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public Long getCommitteeId() {
        return committeeId;
    }

    public void setCommitteeId(Long committeeId) {
        this.committeeId = committeeId;
    }

    public boolean isCommitteeUser() {
        return committeeUser;
    }

    public void setCommitteeUser(boolean committeeUser) {
        this.committeeUser = committeeUser;
    }

    public Long getRepId() {
        return repId;
    }

    public void setRepId(Long repId) {
        this.repId = repId;
    }
}
