package com.sejel.monitor.presentation.controllers.UserManagement;

import com.sejel.monitor.business.beans.UserManagementBean;
import com.sejel.monitor.persistence.entity.userManagement.AmUserProfile;
import com.sejel.monitor.persistence.entity.userManagement.LuRoles;
import com.sejel.monitor.persistence.entity.userManagement.RoleUserMap;
import com.sejel.monitor.presentation.helper.Utilitis.MessagesUtility;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewAccessScoped
public class UserManagementController implements Serializable {
    @Inject
    private UserManagementBean entityBean;
    @Inject
    private MessagesUtility messagesUtility;
    @Inject
    private LoginController loginController;

    private String currentUser;

    private List<AmUserProfile> userProfileList = new ArrayList<>();
    private AmUserProfile newUser;

    @PostConstruct
    public void init() {
        currentUser = loginController.getUserProfile().getuUsername();
        clearFields();
        reloadUsers();
    }


    private void clearFields(){
        newUser = new AmUserProfile();
    }

    public void reloadUsers(){
        userProfileList = entityBean.getAllUsersWithoutCurrentUser(currentUser);
    }

    public String cancel(String page){
        init();
        return "/pages/UserManagement/"+page+".xhtml?faces-redirect=true";
    }

    public String changeUserState(AmUserProfile user){
        entityBean.changeSystemState(user);
        messagesUtility.addSuccessMessageToPage(null, "success.message", "success.message");
        clearFields();
        reloadUsers();
        return "/pages/UserManagement/Users.xhtml?faces-redirect=true";
    }
    public String editUser(AmUserProfile user){
        newUser = user;
        return "/pages/UserManagement/Users.xhtml?faces-redirect=true";
    }

    public String addUser(){
        try {
            if(entityBean.checkUsernameAvailability(newUser)) {
                newUser.setChangeUsername(this.currentUser);
                entityBean.addUser(newUser);
                messagesUtility.addSuccessMessageToPage(null, "success.message", "success.message");
                init();
                return "/pages/UserManagement/Users.xhtml?faces-redirect=true";
            }else{
                messagesUtility.addErrorMessageToPage(null , "user.username.not.available" ,"user.username.not.available");
                return "";
            }
        }catch (Exception e){
            messagesUtility.addErrorMessageToPage(null , "system.error.message" ,"system.error.message");
            return "";
        }
    }


    public List<AmUserProfile> getUserProfileList() {
        return userProfileList;
    }

    public void setUserProfileList(List<AmUserProfile> userProfileList) {
        this.userProfileList = userProfileList;
    }

    public AmUserProfile getNewUser() {
        return newUser;
    }

    public void setNewUser(AmUserProfile newUser) {
        this.newUser = newUser;
    }

    public UserManagementBean getEntityBean() {
        return entityBean;
    }

    public void setEntityBean(UserManagementBean entityBean) {
        this.entityBean = entityBean;
    }

    public LoginController getLoginController() {
        return loginController;
    }

    public void setLoginController(LoginController loginController) {
        this.loginController = loginController;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

}
