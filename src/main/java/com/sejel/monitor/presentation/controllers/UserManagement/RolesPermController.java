package com.sejel.monitor.presentation.controllers.UserManagement;

import com.sejel.monitor.business.beans.RolesPermBean;
import com.sejel.monitor.business.beans.UserManagementBean;
import com.sejel.monitor.persistence.entity.userManagement.*;
import com.sejel.monitor.presentation.helper.Utilitis.MessagesUtility;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewAccessScoped
public class RolesPermController implements Serializable {
    @Inject
    private RolesPermBean entityBean;
    @Inject
    private MessagesUtility messagesUtility;
    @Inject
    private LoginController loginController;

    private String currentUser;
    private LuRoles newRole;
    private List<LuRoles> rolesList = new ArrayList<>();
    private List<LuPermissions> permsList = new ArrayList<>();
    private AmUserProfile newUser;
    private List<String> relatedRolePerm = new ArrayList<>();

    @PostConstruct
    public void init() {
        currentUser = loginController.getUserProfile().getuUsername();
        clearFields();
        reloadRoles();
        reloadPerms();
    }


    private void clearFields() {
        newRole = new LuRoles();
        rolesList = new ArrayList<>();
        permsList = new ArrayList<>();
    }

    public void reloadRoles() {
        rolesList = entityBean.getAllRoles();
    }

    public void reloadPerms() {
        permsList = entityBean.getAllPerms();
    }


    public String rolePermissions(LuRoles role) {
        newRole = role;
        return "/pages/UserManagement/AddPermToRole.xhtml?faces-redirect=true";
    }

    public String userPermissions(AmUserProfile user){
        newUser = user;
        relatedRolePerm = entityBean.getUserPerms(user.getuUsername());
        return "/pages/UserManagement/AddPermToUsers.xhtml?faces-redirect=true";
    }

    public String userRoles(AmUserProfile user){
        newUser = user;
        return "/pages/UserManagement/AddRoleToUsers.xhtml?faces-redirect=true";
    }

    public String cancel(String page) {
        clearFields();
        return "/pages/UserManagement/" + page + ".xhtml?faces-redirect=true";
    }

    public String changeRoleState(LuRoles role) {
        entityBean.changeRoleState(role);
        messagesUtility.addSuccessMessageToPage(null, "success.message", "success.message");
        clearFields();
        reloadRoles();
        return "/pages/UserManagement/Roles.xhtml?faces-redirect=true";
    }

    public String changePermState(LuPermissions perm) {
        entityBean.changePermState(perm);
        messagesUtility.addSuccessMessageToPage(null, "success.message", "success.message");
        clearFields();
        reloadPerms();
        return "/pages/UserManagement/Permissions.xhtml?faces-redirect=true";
    }

    public String editRole(LuRoles role) {
        newRole = role;
        return "/pages/UserManagement/Roles.xhtml?faces-redirect=true";
    }

    public String addRole() {
        try {
            if (entityBean.checkRoleNameAvailability(newRole)) {
                newRole.setChangeUsername(this.currentUser);
                entityBean.addRole(newRole);
                messagesUtility.addSuccessMessageToPage(null, "success.message", "success.message");
                reloadPerms();
                reloadRoles();
                newRole = new LuRoles();
                return "/pages/UserManagement/Roles.xhtml?faces-redirect=true";
            } else {
                messagesUtility.addErrorMessageToPage(null, "user.username.not.available", "user.username.not.available");
                return "";
            }
        } catch (Exception e) {
            messagesUtility.addErrorMessageToPage(null, "system.error.message", "system.error.message");
            return "";
        }
    }

    public boolean checkPermInRole(String perm){
        return relatedRolePerm.contains(perm);
    }
    public boolean checkRolePerm(Long perm) {
        for (RolePermMap temp : newRole.getPermsList()) {
            if (temp.getRpmPermId().equals(perm)) {
                return true;
            }
        }
        return false;
    }

    public boolean checkUserPerm(Long perm) {
        for (PermUserMap temp : newUser.getPermsList()) {
            if (temp.getPumPermId().equals(perm)) {
                return true;
            }
        }
        return false;
    }

    public boolean checkUserRole(Long role) {
        for (RoleUserMap temp : newUser.getRolesList()) {
            if (temp.getRumRoleId().equals(role)) {
                return true;
            }
        }
        return false;
    }

    public String addPermToRole(Long perm) {
        try {
            entityBean.addPermToRole(perm, newRole.getRoleId(), this.currentUser);
            newRole = entityBean.reloadRole(newRole.getRoleId());
            messagesUtility.addSuccessMessageToPage(null, "success.message", "success.message");
            return "/pages/UserManagement/AddPermToRole.xhtml?faces-redirect=true";
        } catch (Exception e) {
            messagesUtility.addErrorMessageToPage(null, "system.error.message", "system.error.message");
            return "";
        }
    }

    public String deletePermFromRole(Long perm) {
        try {
            entityBean.deletePermFromRole(perm, newRole.getRoleId());
            newRole = entityBean.reloadRole(newRole.getRoleId());
            messagesUtility.addSuccessMessageToPage(null, "success.message", "success.message");
            return "/pages/UserManagement/AddPermToRole.xhtml?faces-redirect=true";
        } catch (Exception e) {
            messagesUtility.addErrorMessageToPage(null, "system.error.message", "system.error.message");
            return "";
        }
    }


    public String addPermToUser(Long perm) {
        try {
            entityBean.addPermToUser(perm, newUser.getuId(), this.currentUser);
            newUser = entityBean.reloadUser(newUser.getuId());
            messagesUtility.addSuccessMessageToPage(null, "success.message", "success.message");
            return "/pages/UserManagement/AddPermToUsers.xhtml?faces-redirect=true";
        } catch (Exception e) {
            messagesUtility.addErrorMessageToPage(null, "system.error.message", "system.error.message");
            return "";
        }
    }

    public String deletePermFromUser(Long perm) {
        try {
            entityBean.deletePermFromUser(perm, newUser.getuId());
            newUser = entityBean.reloadUser(newUser.getuId());
            messagesUtility.addSuccessMessageToPage(null, "success.message", "success.message");
            return "/pages/UserManagement/AddPermToUsers.xhtml?faces-redirect=true";
        } catch (Exception e) {
            messagesUtility.addErrorMessageToPage(null, "system.error.message", "system.error.message");
            return "";
        }
    }

    public String addRoleToUser(Long role) {
        try {
            entityBean.addRoleToUser(role, newUser.getuId(), this.currentUser);
            newUser = entityBean.reloadUser(newUser.getuId());
            messagesUtility.addSuccessMessageToPage(null, "success.message", "success.message");
            return "/pages/UserManagement/AddRoleToUsers.xhtml?faces-redirect=true";
        } catch (Exception e) {
            messagesUtility.addErrorMessageToPage(null, "system.error.message", "system.error.message");
            return "";
        }
    }

    public String deleteRoleFromUser(Long role) {
        try {
            entityBean.deleteRoleFromUser(role, newUser.getuId());
            newUser = entityBean.reloadUser(newUser.getuId());
            messagesUtility.addSuccessMessageToPage(null, "success.message", "success.message");
            return "/pages/UserManagement/AddRoleToUsers.xhtml?faces-redirect=true";
        } catch (Exception e) {
            messagesUtility.addErrorMessageToPage(null, "system.error.message", "system.error.message");
            return "";
        }
    }


    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public LuRoles getNewRole() {
        return newRole;
    }

    public void setNewRole(LuRoles newRole) {
        this.newRole = newRole;
    }

    public List<LuRoles> getRolesList() {
        return rolesList;
    }

    public void setRolesList(List<LuRoles> rolesList) {
        this.rolesList = rolesList;
    }

    public List<LuPermissions> getPermsList() {
        return permsList;
    }

    public void setPermsList(List<LuPermissions> permsList) {
        this.permsList = permsList;
    }

    public AmUserProfile getNewUser() {
        return newUser;
    }

    public void setNewUser(AmUserProfile newUser) {
        this.newUser = newUser;
    }

    public List<String> getRelatedRolePerm() {
        return relatedRolePerm;
    }

    public void setRelatedRolePerm(List<String> relatedRolePerm) {
        this.relatedRolePerm = relatedRolePerm;
    }
}
