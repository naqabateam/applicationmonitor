package com.sejel.monitor.presentation.controllers;

import com.sejel.monitor.business.beans.ApplicationMonitorBean;
import com.sejel.monitor.persistence.entity.AmKpis;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Named
@ViewAccessScoped
public class KpiResultsController implements Serializable {

    @Inject
    private ApplicationMonitorBean entityBean;


    private List<String> coulmnsHeaders = new ArrayList<>();

    private List<ColumnModel> columns;
    private List<Object> resaults;

    public void init(AmKpis kpi) {
        resaults = new ArrayList<>();
        resaults = entityBean.getKpiResults(kpi);

        coulmnsHeaders = new ArrayList<>();
        coulmnsHeaders = Arrays.asList(kpi.getColumnsTemplate().split(","));

        createDynamicColumns();
    }

    static public class ColumnModel implements Serializable {

        private String header;
        private String property;

        ColumnModel(String header, String property) {
            this.header = header;
            this.property = property;
        }

        public String getHeader() {
            return header;
        }

        public String getProperty() {
            return property;
        }
    }


    private void createDynamicColumns() {

        columns = new ArrayList<ColumnModel>();

        for(int i = 0 ; i<coulmnsHeaders.size(); i++){
            columns.add(new ColumnModel(coulmnsHeaders.get(i), String.valueOf(i)));
        }
    }

    public ApplicationMonitorBean getEntityBean() {
        return entityBean;
    }

    public void setEntityBean(ApplicationMonitorBean entityBean) {
        this.entityBean = entityBean;
    }

    public List<String> getCoulmnsHeaders() {
        return coulmnsHeaders;
    }

    public void setCoulmnsHeaders(List<String> coulmnsHeaders) {
        this.coulmnsHeaders = coulmnsHeaders;
    }

    public List<ColumnModel> getColumns() {
        return columns;
    }

    public void setColumns(List<ColumnModel> columns) {
        this.columns = columns;
    }

    public List<Object> getResaults() {
        return resaults;
    }

    public void setResaults(List<Object> resaults) {
        this.resaults = resaults;
    }
}