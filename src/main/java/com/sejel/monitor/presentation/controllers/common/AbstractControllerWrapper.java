package com.sejel.monitor.presentation.controllers.common;

import com.sejel.common.controller.AbstractPrimefacesController;
import com.sejel.monitor.presentation.controllers.UserManagement.LoginController;
import com.sejel.monitor.presentation.helper.Utilitis.MessagesUtility;
import com.sejel.monitor.presentation.helper.Utilitis.SmsUtility;

import javax.inject.Inject;

public abstract class AbstractControllerWrapper<T> extends AbstractPrimefacesController<T> {

    @Inject
    protected LoginController loginController;
    @Inject
    protected MessagesUtility messagesUtility;
    @Inject
    protected SmsUtility smsUtility;

}
