/*
 * Generated, Do Not Modify
 */
/*
 * Copyright 2009-2013 PrimeTek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sejel.monitor.custom.primefaces;

import org.primefaces.PrimeFaces;
import org.primefaces.component.menuitem.UIMenuItem;
import org.primefaces.component.submenu.UISubmenu;

import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.component.visit.VisitCallback;
import javax.faces.component.visit.VisitContext;
import javax.faces.component.visit.VisitResult;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;

@Named(value = "menuHandler")
@SessionScoped
public class MenuHandler implements Serializable {


  /**
   * Init render sideBar Menu
   */
  public void renderSideBar() {

    UIComponent mainMenu = findComponent("mainMenu");
    for (UIComponent component : mainMenu.getChildren()) {
      boolean isRendered = false;
      if (component.getId()!= null && component.getId().contains("subMenu") && component.isRendered()) {
        isRendered = iterateChildren(component);
        for (UIComponent componentChild : component.getChildren()) {
          if (componentChild.getId()!= null && componentChild.getId().contains("subMenu") && componentChild.isRendered()) {
            component.setRendered(true);
            break;
          }
        }
        PrimeFaces.current().ajax().update();
      }
    }

  }

  /**
   * Iterate on Component Child
   * @param component
   * @return
   */
  private boolean iterateChildren(UIComponent component) {
    boolean isRendered = false;
    ((UISubmenu) component).setRendered(false);
    for (UIComponent subMenuChild : component.getChildren()) {//this is the child of panelMenu ie Submenu
      if (subMenuChild.getId() != null && subMenuChild.getId().contains("subMenu") && subMenuChild.isRendered()) {
        isRendered = iterateChildren(subMenuChild);
        subMenuChild.setRendered(isRendered);
      } else if (subMenuChild instanceof UIMenuItem) {
        if(subMenuChild.isRendered()) {
          ((UISubmenu) component).setRendered(true);
          isRendered = true;
        }
      }
    }
    ((UISubmenu) component).setRendered(isRendered);
    return isRendered;
  }

  /**
   * Search a component by ID
   * @param menuId
   * @return Component found or not
   */
  private UIComponent findComponent(String menuId) {
    FacesContext context = FacesContext.getCurrentInstance();
    VisitContext visitContext = VisitContext.createVisitContext(context);
    UIViewRoot root = context.getViewRoot();
    UIComponent[] found = new UIComponent[1];

    root.visitTree( visitContext, new VisitCallback() {
      @Override
      public VisitResult visit(VisitContext context, UIComponent component) {
        if(menuId.equals(component.getId())){
          found[0] = component;
          return VisitResult.COMPLETE;
        }
        return VisitResult.ACCEPT;
      }
    });

    return found[0];

  }
}
