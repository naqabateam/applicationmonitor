/*
 * Generated, Do Not Modify
 */
/*
 * Copyright 2009-2013 PrimeTek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sejel.monitor.custom.primefaces;

import org.primefaces.component.api.UIData;

public class DataTable extends org.primefaces.component.datatable.DataTable {

  @Override
  public boolean isPaginator() {
    return (Boolean) getStateHelper().eval(UIData.PropertyKeys.paginator, true);
  }

  @Override
  public String getPaginatorTemplate() {
    return (String) getStateHelper().eval(UIData.PropertyKeys.paginatorTemplate,
        "{CurrentPageReport} {FirstPageLink} {PreviousPageLink} {PageLinks} {NextPageLink} {LastPageLink} {RowsPerPageDropdown}");
  }

  @Override
  public String getRowsPerPageTemplate() {
    return (String) getStateHelper().eval(UIData.PropertyKeys.rowsPerPageTemplate, "10,25,50,100");
  }

  @Override
  public String getPaginatorPosition() {
    return (String) getStateHelper().eval(UIData.PropertyKeys.paginatorPosition, "bottom");
  }

    /*@Override
    public String getSelectionMode() {
        return (String) getStateHelper().eval(PropertyKeys.selectionMode, "single");
    }*/

  @Override
  public boolean isLazy() {
    return (Boolean) getStateHelper().eval(UIData.PropertyKeys.lazy, true);
  }

  public String getWidgetVar() {
    return (String) this.getStateHelper()
        .eval(org.primefaces.component.datatable.DataTable.PropertyKeys.widgetVar, getId());
  }
   /* @Override
    public boolean isDisabledTextSelection() {
        return (Boolean) getStateHelper().eval(PropertyKeys.disabledTextSelection, false);
    }

    @Override
    public boolean isDisabledSelection() {
        return (Boolean) getStateHelper().eval(PropertyKeys.disabledSelection, true);
    }*/


}