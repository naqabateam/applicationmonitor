package com.sejel.monitor.persistence.helper.qualifire;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class EnityManager {
    @Produces
    @MAINUNIT
    @PersistenceContext(name = "MainUnit")
    private EntityManager entityManager;
}
