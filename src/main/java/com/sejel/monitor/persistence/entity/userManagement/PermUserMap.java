package com.sejel.monitor.persistence.entity.userManagement;

import com.sejel.common.jpa.JPAAbstractRoot;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "perm_user_map", schema = "app_monitor", catalog = "tdcdb")
public class PermUserMap extends JPAAbstractRoot implements Serializable {
    @Id
    @Column(name = "pum_id", nullable = false)
    private Long pumId;
    
    @Column(name = "pum_perm_id", nullable = false)
    private Long pumPermId;
    
    @Column(name = "pum_user_id", nullable = false)
    private Long pumUserId;
    
    @Column(name = "pum_state", nullable = true)
    private Integer pumState;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_timestamp", nullable = true)
    private Date createTimestamp;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "change_timestamp", nullable = true)
    private Date changeTimestamp;
    
    @Column(name = "change_username", nullable = true, length = 200)
    private String changeUsername;

    @ManyToOne
    @JoinColumn(name = "pum_user_id", referencedColumnName = "u_id", insertable = false, updatable = false)
    private AmUserProfile user;

    @ManyToOne
    @JoinColumn(name = "pum_perm_id", referencedColumnName = "p_id", insertable = false, updatable = false)
    private LuPermissions permission;

    public AmUserProfile getUser() {
        return user;
    }

    public void setUser(AmUserProfile user) {
        this.user = user;
    }

    public LuPermissions getPermission() {
        return permission;
    }

    public void setPermission(LuPermissions permission) {
        this.permission = permission;
    }

    public Long getPumId() {
        return pumId;
    }

    public void setPumId(Long pumId) {
        this.pumId = pumId;
    }

    public Long getPumPermId() {
        return pumPermId;
    }

    public void setPumPermId(Long pumPermId) {
        this.pumPermId = pumPermId;
    }

    public Long getPumUserId() {
        return pumUserId;
    }

    public void setPumUserId(Long pumUserId) {
        this.pumUserId = pumUserId;
    }

    public Integer getPumState() {
        return pumState;
    }

    public void setPumState(Integer pumState) {
        this.pumState = pumState;
    }

    public Date getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(Date createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public Date getChangeTimestamp() {
        return changeTimestamp;
    }

    public void setChangeTimestamp(Date changeTimestamp) {
        this.changeTimestamp = changeTimestamp;
    }

    public String getChangeUsername() {
        return changeUsername;
    }

    public void setChangeUsername(String changeUsername) {
        this.changeUsername = changeUsername;
    }

    @Override
    public Object getId() {
        return this.pumId;
    }

    @Override
    public void setId(Object key) {

    }

    @Override
    public String getSequenceIdQuery() {
        return "select nextval('app_monitor.perm_user_map_pum_id_seq')";
    }

    @Override
    public Object getNewID() {
        return null;
    }

    @Override
    public String getFullActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortListNamedQ() {
        return null;
    }

    @Override
    public int getDBEntityID() {
        return 0;
    }
}
