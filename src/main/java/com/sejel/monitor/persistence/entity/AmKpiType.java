package com.sejel.monitor.persistence.entity;

import com.sejel.common.jpa.JPAAbstractRoot;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "am_kpi_type", schema = "app_monitor", catalog = "tdcdb")
public class AmKpiType extends JPAAbstractRoot implements Serializable {
    @Id
    @Column(name = "kpi_type_id", nullable = false)
    private Integer kpiTypeId;
    
    @Column(name = "kpi_type_name", nullable = true, length = 200)
    private String kpiTypeName;
    
    @Column(name = "kpi_state", nullable = true)
    private Integer kpiState;

    public Integer getKpiTypeId() {
        return kpiTypeId;
    }

    public void setKpiTypeId(Integer kpiTypeId) {
        this.kpiTypeId = kpiTypeId;
    }

    public String getKpiTypeName() {
        return kpiTypeName;
    }

    public void setKpiTypeName(String kpiTypeName) {
        this.kpiTypeName = kpiTypeName;
    }

    public Integer getKpiState() {
        return kpiState;
    }

    public void setKpiState(Integer kpiState) {
        this.kpiState = kpiState;
    }
    
    @Override
    public Object getId() {
        return this.kpiTypeId;
    }

    @Override
    public void setId(Object key) {

    }

    @Override
    public String getSequenceIdQuery() {
        return null;
    }

    @Override
    public Object getNewID() {
        return null;
    }

    @Override
    public String getFullActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortListNamedQ() {
        return null;
    }

    @Override
    public int getDBEntityID() {
        return 0;
    }
}
