package com.sejel.monitor.persistence.entity.userManagement;

import com.sejel.common.jpa.JPAAbstractRoot;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "lu_roles", schema = "app_monitor", catalog = "tdcdb")
public class LuRoles extends JPAAbstractRoot implements Serializable {
    @Id
    @Column(name = "role_id", nullable = false)
    private Long roleId;
    
    @Column(name = "role_name_ar", nullable = true, length = 200)
    private String roleNameAr;
    
    @Column(name = "role_name_la", nullable = true, length = 200)
    private String roleNameLa;
    
    @Column(name = "role_state", nullable = true)
    private Integer roleState;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_timestamp", nullable = true)
    private Date createTimestamp;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "change_timestamp", nullable = true)
    private Date changeTimestamp;
    
    @Column(name = "change_username", nullable = true, length = 200)
    private String changeUsername;

    @OneToMany
    @JoinColumn(name = "rum_role_id", referencedColumnName = "role_id", insertable = false, updatable = false)
    private List<RoleUserMap> usersList;

    @OneToMany
    @JoinColumn(name = "rpm_role_id", referencedColumnName = "role_id", insertable = false, updatable = false)
    private List<RolePermMap> permsList;

    public List<RoleUserMap> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<RoleUserMap> usersList) {
        this.usersList = usersList;
    }

    public List<RolePermMap> getPermsList() {
        return permsList;
    }

    public void setPermsList(List<RolePermMap> permsList) {
        this.permsList = permsList;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleNameAr() {
        return roleNameAr;
    }

    public void setRoleNameAr(String roleNameAr) {
        this.roleNameAr = roleNameAr;
    }

    public String getRoleNameLa() {
        return roleNameLa;
    }

    public void setRoleNameLa(String roleNameLa) {
        this.roleNameLa = roleNameLa;
    }

    public Integer getRoleState() {
        return roleState;
    }

    public void setRoleState(Integer roleState) {
        this.roleState = roleState;
    }

    public Date getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(Date createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public Date getChangeTimestamp() {
        return changeTimestamp;
    }

    public void setChangeTimestamp(Date changeTimestamp) {
        this.changeTimestamp = changeTimestamp;
    }

    public String getChangeUsername() {
        return changeUsername;
    }

    public void setChangeUsername(String changeUsername) {
        this.changeUsername = changeUsername;
    }

    @Override
    public Object getId() {
        return this.roleId;
    }

    @Override
    public void setId(Object key) {

    }

    @Override
    public String getSequenceIdQuery() {
        return "select nextval('app_monitor.lu_roles_role_id_seq')";
    }

    @Override
    public Object getNewID() {
        return null;
    }

    @Override
    public String getFullActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortListNamedQ() {
        return null;
    }

    @Override
    public int getDBEntityID() {
        return 0;
    }
}
