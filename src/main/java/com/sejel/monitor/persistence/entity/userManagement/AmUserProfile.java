package com.sejel.monitor.persistence.entity.userManagement;

import com.sejel.common.jpa.JPAAbstractRoot;
import com.sejel.monitor.persistence.entity.AmKpis;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "user_profile", schema = "app_monitor", catalog = "tdcdb")
public class AmUserProfile extends JPAAbstractRoot implements Serializable {
    @Id
    @Column(name = "u_id", nullable = false)
    private Long uId;
    
    @Column(name = "u_name_ar", nullable = true, length = 200)
    private String uNameAr;
    
    @Column(name = "u_name_la", nullable = true, length = 200)
    private String uNameLa;
    
    @Column(name = "u_username")
    private String uUsername;
    
    @Column(name = "u_password")
    private String uPassword;
    
    @Column(name = "u_phone_number", nullable = true, length = 15)
    private String uPhoneNumber;
    
    @Column(name = "u_state")
    private Integer uState;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_timestamp", nullable = true)
    private Date createTimestamp;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "change_timestamp", nullable = true)
    private Date changeTimestamp;
    
    @Column(name = "change_username", nullable = true, length = 200)
    private String changeUsername;

    @OneToMany
    @JoinColumn(name = "pum_user_id", referencedColumnName = "u_id", insertable = false, updatable = false)
    private List<PermUserMap> permsList;

    @OneToMany
    @JoinColumn(name = "rum_user_id", referencedColumnName = "u_id", insertable = false, updatable = false)
    private List<RoleUserMap> rolesList;

    public List<PermUserMap> getPermsList() {
        return permsList;
    }

    public void setPermsList(List<PermUserMap> permsList) {
        this.permsList = permsList;
    }

    public List<RoleUserMap> getRolesList() {
        return rolesList;
    }

    public void setRolesList(List<RoleUserMap> rolesList) {
        this.rolesList = rolesList;
    }

    public Long getuId() {
        return uId;
    }

    public void setuId(Long uId) {
        this.uId = uId;
    }

    public String getuNameAr() {
        return uNameAr;
    }

    public void setuNameAr(String uNameAr) {
        this.uNameAr = uNameAr;
    }

    public String getuNameLa() {
        return uNameLa;
    }

    public void setuNameLa(String uNameLa) {
        this.uNameLa = uNameLa;
    }

    public String getuUsername() {
        return uUsername;
    }

    public void setuUsername(String uUsername) {
        this.uUsername = uUsername;
    }

    public String getuPassword() {
        return uPassword;
    }

    public void setuPassword(String uPassword) {
        this.uPassword = uPassword;
    }

    public String getuPhoneNumber() {
        return uPhoneNumber;
    }

    public void setuPhoneNumber(String uPhoneNumber) {
        this.uPhoneNumber = uPhoneNumber;
    }

    public Integer getuState() {
        return uState;
    }

    public void setuState(Integer uState) {
        this.uState = uState;
    }

    public Date getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(Date createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public Date getChangeTimestamp() {
        return changeTimestamp;
    }

    public void setChangeTimestamp(Date changeTimestamp) {
        this.changeTimestamp = changeTimestamp;
    }

    public String getChangeUsername() {
        return changeUsername;
    }

    public void setChangeUsername(String changeUsername) {
        this.changeUsername = changeUsername;
    }

    @Override
    public Object getId() {
        return this.uId;
    }

    @Override
    public void setId(Object key) {

    }

    @Override
    public String getSequenceIdQuery() {
        return "select nextval('app_monitor.user_profile_u_id_seq')";
    }

    @Override
    public Object getNewID() {
        return null;
    }

    @Override
    public String getFullActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortListNamedQ() {
        return null;
    }

    @Override
    public int getDBEntityID() {
        return 0;
    }
}
