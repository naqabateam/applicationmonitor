package com.sejel.monitor.persistence.entity;

import com.sejel.common.jpa.JPAAbstractRoot;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "am_systems", schema = "app_monitor", catalog = "tdcdb")
public class AmSystems  extends JPAAbstractRoot implements Serializable {
    @Id
    @Column(name = "sys_id", nullable = false)
    private Long sysId;
    
    @Column(name = "sys_name", nullable = true, length = 200)
    private String sysName;
    
    @Column(name = "sys_events_count", nullable = true)
    private Integer sysEventsCount;
    
    @Column(name = "sys_state", nullable = true)
    private Integer sysState;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_timestamp", nullable = true)
    private Date createTimestamp;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "change_timestamp", nullable = true)
    private Date changeTimestamp;
    
    @Column(name = "change_username", nullable = true, length = 200)
    private String changeUsername;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "grp_sys_id", referencedColumnName = "sys_id", insertable = false, updatable = false)
    private List<AmGroups> groupsList;

    public List<AmGroups> getGroupsList() {
        return groupsList;
    }

    public void setGroupsList(List<AmGroups> groupsList) {
        this.groupsList = groupsList;
    }

    public Long getSysId() {
        return sysId;
    }

    public void setSysId(Long sysId) {
        this.sysId = sysId;
    }

    public String getSysName() {
        return sysName;
    }

    public void setSysName(String sysName) {
        this.sysName = sysName;
    }

    public Integer getSysEventsCount() {
        return sysEventsCount;
    }

    public void setSysEventsCount(Integer sysEventsCount) {
        this.sysEventsCount = sysEventsCount;
    }

    public Integer getSysState() {
        return sysState;
    }

    public void setSysState(Integer sysState) {
        this.sysState = sysState;
    }

    public Date getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(Date createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public Date getChangeTimestamp() {
        return changeTimestamp;
    }

    public void setChangeTimestamp(Date changeTimestamp) {
        this.changeTimestamp = changeTimestamp;
    }

    public String getChangeUsername() {
        return changeUsername;
    }

    public void setChangeUsername(String changeUsername) {
        this.changeUsername = changeUsername;
    }

    @Override
    public Object getId() {
        return this.sysId;
    }

    @Override
    public void setId(Object key) {

    }

    @Override
    public String getSequenceIdQuery() {
        return "select nextval('app_monitor.am_systems_sys_id_seq')";
    }

    @Override
    public Object getNewID() {
        return null;
    }

    @Override
    public String getFullActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortListNamedQ() {
        return null;
    }

    @Override
    public int getDBEntityID() {
        return 0;
    }
}
