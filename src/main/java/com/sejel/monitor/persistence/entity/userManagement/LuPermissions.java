package com.sejel.monitor.persistence.entity.userManagement;

import com.sejel.common.jpa.JPAAbstractRoot;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "lu_permissions", schema = "app_monitor", catalog = "tdcdb")
public class LuPermissions  extends JPAAbstractRoot implements Serializable {
    @Id
    @Column(name = "p_id", nullable = false)
    private Long pId;
    
    @Column(name = "p_name_ar", nullable = true, length = 200)
    private String pNameAr;
    
    @Column(name = "p_name_la", nullable = true, length = 200)
    private String pNameLa;
    
    @Column(name = "p_state", nullable = true)
    private Integer pState;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_timestamp", nullable = true)
    private Date createTimestamp;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "change_timestamp", nullable = true)
    private Date changeTimestamp;
    
    @Column(name = "change_username", nullable = true, length = 200)
    private String changeUsername;

    @OneToMany
    @JoinColumn(name = "pum_perm_id", referencedColumnName = "p_id", insertable = false, updatable = false)
    private List<PermUserMap> usersList;

    @OneToMany
    @JoinColumn(name = "rpm_perm_id", referencedColumnName = "p_id", insertable = false, updatable = false)
    private List<RolePermMap> rolesList;

    public List<PermUserMap> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<PermUserMap> usersList) {
        this.usersList = usersList;
    }

    public List<RolePermMap> getRolesList() {
        return rolesList;
    }

    public void setRolesList(List<RolePermMap> rolesList) {
        this.rolesList = rolesList;
    }

    public Long getpId() {
        return pId;
    }

    public void setpId(Long pId) {
        this.pId = pId;
    }

    public String getpNameAr() {
        return pNameAr;
    }

    public void setpNameAr(String pNameAr) {
        this.pNameAr = pNameAr;
    }

    public String getpNameLa() {
        return pNameLa;
    }

    public void setpNameLa(String pNameLa) {
        this.pNameLa = pNameLa;
    }

    public Integer getpState() {
        return pState;
    }

    public void setpState(Integer pState) {
        this.pState = pState;
    }

    public Date getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(Date createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public Date getChangeTimestamp() {
        return changeTimestamp;
    }

    public void setChangeTimestamp(Date changeTimestamp) {
        this.changeTimestamp = changeTimestamp;
    }

    public String getChangeUsername() {
        return changeUsername;
    }

    public void setChangeUsername(String changeUsername) {
        this.changeUsername = changeUsername;
    }

    @Override
    public Object getId() {
        return this.pId;
    }

    @Override
    public void setId(Object key) {

    }

    @Override
    public String getSequenceIdQuery() {
        return "select nextval('app_monitor.lu_permissions_p_id_seq')";
    }

    @Override
    public Object getNewID() {
        return null;
    }

    @Override
    public String getFullActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortListNamedQ() {
        return null;
    }

    @Override
    public int getDBEntityID() {
        return 0;
    }
}
