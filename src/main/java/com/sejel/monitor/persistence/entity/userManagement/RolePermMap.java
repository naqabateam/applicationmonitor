package com.sejel.monitor.persistence.entity.userManagement;

import com.sejel.common.jpa.JPAAbstractRoot;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "role_perm_map", schema = "app_monitor", catalog = "tdcdb")
public class RolePermMap extends JPAAbstractRoot implements Serializable {
    @Id
    @Column(name = "rpm_id", nullable = false)
    private Long rpmId;
    
    @Column(name = "rpm_role_id", nullable = false)
    private Long rpmRoleId;
    
    @Column(name = "rpm_perm_id", nullable = false)
    private Long rpmPermId;
    
    @Column(name = "rpm_state", nullable = true)
    private Integer rpmState;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_timestamp", nullable = true)
    private Date createTimestamp;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "change_timestamp", nullable = true)
    private Date changeTimestamp;
    
    @Column(name = "change_username", nullable = true, length = 200)
    private String changeUsername;

    @ManyToOne
    @JoinColumn(name = "rpm_role_id", referencedColumnName = "role_id", insertable = false, updatable = false)
    private LuRoles role;

    @ManyToOne
    @JoinColumn(name = "rpm_perm_id", referencedColumnName = "p_id", insertable = false, updatable = false)
    private LuPermissions permission;

    public LuRoles getRole() {
        return role;
    }

    public void setRole(LuRoles role) {
        this.role = role;
    }

    public LuPermissions getPermission() {
        return permission;
    }

    public void setPermission(LuPermissions permission) {
        this.permission = permission;
    }

    public Long getRpmId() {
        return rpmId;
    }

    public void setRpmId(Long rpmId) {
        this.rpmId = rpmId;
    }

    public Long getRpmRoleId() {
        return rpmRoleId;
    }

    public void setRpmRoleId(Long rpmRoleId) {
        this.rpmRoleId = rpmRoleId;
    }

    public Long getRpmPermId() {
        return rpmPermId;
    }

    public void setRpmPermId(Long rpmPermId) {
        this.rpmPermId = rpmPermId;
    }

    public Integer getRpmState() {
        return rpmState;
    }

    public void setRpmState(Integer rpmState) {
        this.rpmState = rpmState;
    }

    public Date getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(Date createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public Date getChangeTimestamp() {
        return changeTimestamp;
    }

    public void setChangeTimestamp(Date changeTimestamp) {
        this.changeTimestamp = changeTimestamp;
    }

    public String getChangeUsername() {
        return changeUsername;
    }

    public void setChangeUsername(String changeUsername) {
        this.changeUsername = changeUsername;
    }

    @Override
    public Object getId() {
        return this.rpmId;
    }

    @Override
    public void setId(Object key) {

    }

    @Override
    public String getSequenceIdQuery() {
        return "select nextval('app_monitor.role_perm_map_rpm_id_seq')";
    }

    @Override
    public Object getNewID() {
        return null;
    }

    @Override
    public String getFullActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortListNamedQ() {
        return null;
    }

    @Override
    public int getDBEntityID() {
        return 0;
    }
}
