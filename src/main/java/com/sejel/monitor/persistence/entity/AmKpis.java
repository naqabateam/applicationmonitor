package com.sejel.monitor.persistence.entity;

import com.sejel.common.jpa.JPAAbstractRoot;
import com.sejel.monitor.persistence.entity.userManagement.AmUserProfile;
import org.eclipse.persistence.annotations.AdditionalCriteria;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "am_kpis", schema = "app_monitor", catalog = "tdcdb")
public class AmKpis extends JPAAbstractRoot implements Serializable {
    @Id
    @Column(name = "kpi_id")
    private Long kpiId;
    
    @Column(name = "kpi_name")
    private String kpiName;
    
    @Column(name = "kpi_grp_id")
    private Long kpiGrpId;
    
    @Column(name = "kpi_events_count")
    private Integer kpiEventsCount;
    
    @Column(name = "kpi_state")
    private Integer kpiState;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_timestamp")
    private Date createTimestamp;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "change_timestamp")
    private Date changeTimestamp;
    
    @Column(name = "change_username")
    private String changeUsername;

    @Column(name = "kpi_check_query")
    private String kpiCheckQuery;

    @Column(name = "kpi_result_query")
    private String kpiResultQuery;

    @Column(name = "columns_template")
    private String columnsTemplate;

    @Column(name = "kpi_type_id")
    private Integer kpiTypeId;

    @Column(name = "kpi_refresh_time_mins")
    private Integer kpiRefreshTimeMins;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "kpi_last_refresh_timestamp")
    private Date kpiLastRefreshTimestamp;
    

    @ManyToOne
    @JoinColumn(name = "kpi_grp_id", referencedColumnName = "grp_id", insertable = false, updatable = false)
    private AmGroups group;

    @ManyToOne
    @JoinColumn(name = "kpi_type_id", referencedColumnName = "kpi_type_id", insertable = false, updatable = false)
    private AmKpiType kpiType;

    @Column(name = "kpi_user_id", nullable = true)
    private Long eventUserId;

    @ManyToOne
    @JoinColumn(name = "kpi_user_id", referencedColumnName = "u_id", insertable = false, updatable = false)
    private AmUserProfile userProfile;

    public String getColumnsTemplate() {
        return columnsTemplate;
    }

    public void setColumnsTemplate(String columnsTemplate) {
        this.columnsTemplate = columnsTemplate;
    }

    public Long getEventUserId() {
        return eventUserId;
    }

    public void setEventUserId(Long eventUserId) {
        this.eventUserId = eventUserId;
    }

    public AmUserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(AmUserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public Integer getKpiRefreshTimeMins() {
        return kpiRefreshTimeMins;
    }

    public void setKpiRefreshTimeMins(Integer kpiRefreshTimeMins) {
        this.kpiRefreshTimeMins = kpiRefreshTimeMins;
    }

    public Date getKpiLastRefreshTimestamp() {
        return kpiLastRefreshTimestamp;
    }

    public void setKpiLastRefreshTimestamp(Date kpiLastRefreshTimestamp) {
        this.kpiLastRefreshTimestamp = kpiLastRefreshTimestamp;
    }

    public AmGroups getGroup() {
        return group;
    }

    public void setGroup(AmGroups group) {
        this.group = group;
    }

    public AmKpiType getKpiType() {
        return kpiType;
    }

    public void setKpiType(AmKpiType kpiType) {
        this.kpiType = kpiType;
    }

    public Integer getKpiTypeId() {
        return kpiTypeId;
    }

    public void setKpiTypeId(Integer kpiTypeId) {
        this.kpiTypeId = kpiTypeId;
    }

    public String getKpiCheckQuery() {
        return kpiCheckQuery;
    }

    public void setKpiCheckQuery(String kpiCheckQuery) {
        this.kpiCheckQuery = kpiCheckQuery;
    }

    public String getKpiResultQuery() {
        return kpiResultQuery;
    }

    public void setKpiResultQuery(String kpiResultQuery) {
        this.kpiResultQuery = kpiResultQuery;
    }

    public Long getKpiId() {
        return kpiId;
    }

    public void setKpiId(Long kpiId) {
        this.kpiId = kpiId;
    }

    public String getKpiName() {
        return kpiName;
    }

    public void setKpiName(String kpiName) {
        this.kpiName = kpiName;
    }

    public Long getKpiGrpId() {
        return kpiGrpId;
    }

    public void setKpiGrpId(Long kpiGrpId) {
        this.kpiGrpId = kpiGrpId;
    }

    public Integer getKpiEventsCount() {
        return kpiEventsCount;
    }

    public void setKpiEventsCount(Integer kpiEventsCount) {
        this.kpiEventsCount = kpiEventsCount;
    }

    public Integer getKpiState() {
        return kpiState;
    }

    public void setKpiState(Integer kpiState) {
        this.kpiState = kpiState;
    }

    public Date getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(Date createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public Date getChangeTimestamp() {
        return changeTimestamp;
    }

    public void setChangeTimestamp(Date changeTimestamp) {
        this.changeTimestamp = changeTimestamp;
    }

    public String getChangeUsername() {
        return changeUsername;
    }

    public void setChangeUsername(String changeUsername) {
        this.changeUsername = changeUsername;
    }

    @Override
    public Object getId() {
        return this.kpiId;
    }

    @Override
    public void setId(Object key) {

    }

    @Override
    public String getSequenceIdQuery() {
        return "select nextval('app_monitor.am_kpis_kpi_id_seq')";
    }

    @Override
    public Object getNewID() {
        return null;
    }

    @Override
    public String getFullActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortListNamedQ() {
        return null;
    }

    @Override
    public int getDBEntityID() {
        return 0;
    }
}
