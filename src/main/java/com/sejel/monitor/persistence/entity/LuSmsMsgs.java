package com.sejel.monitor.persistence.entity;

import com.sejel.common.jpa.JPAAbstractRoot;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "lu_sms_msgs", schema = "sejel_systems", catalog = "tdcdb")
public class LuSmsMsgs extends JPAAbstractRoot implements Serializable {
    @Id
    @Column(name = "sms_id")
    private Long smsId;
    @Basic
    @Column(name = "sms_sender")
    private String smsSender;
    @Basic
    @Column(name = "sms_receivers")
    private String smsReceivers;
    @Basic
    @Column(name = "sms_timestamp_la")
    @Temporal(TemporalType.TIMESTAMP)
    private Date smsTimestampLa;
    @Basic
    @Column(name = "sms_execution_timestamp_la")
    @Temporal(TemporalType.TIMESTAMP)
    private Date smsExecutionTimestampLa;
    @Basic
    @Column(name = "sms_processing_status")
    private String smsProcessingStatus;
    @Basic
    @Column(name = "sms_processing_timestamp_la")
    @Temporal(TemporalType.TIMESTAMP)
    private Date smsProcessingTimestampLa;
    @Basic
    @Column(name = "sms_username")
    private String smsUsername;
    @Basic
    @Column(name = "sms_comment")
    private String smsComment;
    @Basic
    @Column(name = "sms_body")
    private String smsBody;
    @Basic
    @Column(name = "referenece_id")
    private String refereneceId;
    @Basic
    @Column(name = "sms_node_id")
    private Long smsNodeId;
    @Basic
    @Column(name = "sms_instance_id")
    private Long smsInstanceId;

    public Long getSmsId() {
        return smsId;
    }

    public void setSmsId(Long smsId) {
        this.smsId = smsId;
    }

    public String getSmsSender() {
        return smsSender;
    }

    public void setSmsSender(String smsSender) {
        this.smsSender = smsSender;
    }

    public String getSmsReceivers() {
        return smsReceivers;
    }

    public void setSmsReceivers(String smsReceivers) {
        this.smsReceivers = smsReceivers;
    }

    public Date getSmsTimestampLa() {
        return smsTimestampLa;
    }

    public void setSmsTimestampLa(Date smsTimestampLa) {
        this.smsTimestampLa = smsTimestampLa;
    }

    public Date getSmsExecutionTimestampLa() {
        return smsExecutionTimestampLa;
    }

    public void setSmsExecutionTimestampLa(Date smsExecutionTimestampLa) {
        this.smsExecutionTimestampLa = smsExecutionTimestampLa;
    }

    public String getSmsProcessingStatus() {
        return smsProcessingStatus;
    }

    public void setSmsProcessingStatus(String smsProcessingStatus) {
        this.smsProcessingStatus = smsProcessingStatus;
    }

    public Date getSmsProcessingTimestampLa() {
        return smsProcessingTimestampLa;
    }

    public void setSmsProcessingTimestampLa(Date smsProcessingTimestampLa) {
        this.smsProcessingTimestampLa = smsProcessingTimestampLa;
    }

    public String getSmsUsername() {
        return smsUsername;
    }

    public void setSmsUsername(String smsUsername) {
        this.smsUsername = smsUsername;
    }

    public String getSmsComment() {
        return smsComment;
    }

    public void setSmsComment(String smsComment) {
        this.smsComment = smsComment;
    }

    public String getSmsBody() {
        return smsBody;
    }

    public void setSmsBody(String smsBody) {
        this.smsBody = smsBody;
    }

    public String getRefereneceId() {
        return refereneceId;
    }

    public void setRefereneceId(String refereneceId) {
        this.refereneceId = refereneceId;
    }

    public Long getSmsNodeId() {
        return smsNodeId;
    }

    public void setSmsNodeId(Long smsNodeId) {
        this.smsNodeId = smsNodeId;
    }

    public Long getSmsInstanceId() {
        return smsInstanceId;
    }

    public void setSmsInstanceId(Long smsInstanceId) {
        this.smsInstanceId = smsInstanceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LuSmsMsgs luSmsMsgs = (LuSmsMsgs) o;
        return Objects.equals(smsId, luSmsMsgs.smsId) &&
                Objects.equals(smsSender, luSmsMsgs.smsSender) &&
                Objects.equals(smsReceivers, luSmsMsgs.smsReceivers) &&
                Objects.equals(smsTimestampLa, luSmsMsgs.smsTimestampLa) &&
                Objects.equals(smsExecutionTimestampLa, luSmsMsgs.smsExecutionTimestampLa) &&
                Objects.equals(smsProcessingStatus, luSmsMsgs.smsProcessingStatus) &&
                Objects.equals(smsProcessingTimestampLa, luSmsMsgs.smsProcessingTimestampLa) &&
                Objects.equals(smsUsername, luSmsMsgs.smsUsername) &&
                Objects.equals(smsComment, luSmsMsgs.smsComment) &&
                Objects.equals(smsBody, luSmsMsgs.smsBody) &&
                Objects.equals(refereneceId, luSmsMsgs.refereneceId) &&
                Objects.equals(smsNodeId, luSmsMsgs.smsNodeId) &&
                Objects.equals(smsInstanceId, luSmsMsgs.smsInstanceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(smsId, smsSender, smsReceivers, smsTimestampLa, smsExecutionTimestampLa, smsProcessingStatus, smsProcessingTimestampLa, smsUsername, smsComment, smsBody, refereneceId, smsNodeId, smsInstanceId);
    }

    @Override
    public Object getId() {
        return this.smsId;
    }

    @Override
    public void setId(Object key) {
        this.smsId = (Long) key;
    }

    @Override
    public String getSequenceIdQuery() {
        return "select nextval('sejel_systems.lu_sms_msgs_sms_id_seq'::regclass)";
    }

    @Override
    public Object getNewID() {
        return null;
    }

    @Override
    public String getFullActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortListNamedQ() {
        return null;
    }

    @Override
    public int getDBEntityID() {
        return 0;
    }
}
