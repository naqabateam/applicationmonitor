package com.sejel.monitor.persistence.entity.userManagement;

import com.sejel.common.jpa.JPAAbstractRoot;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "role_user_map", schema = "app_monitor", catalog = "tdcdb")
public class RoleUserMap extends JPAAbstractRoot implements Serializable {
    @Id
    @Column(name = "rum_id", nullable = false)
    private Long rumId;
    
    @Column(name = "rum_role_id", nullable = false)
    private Long rumRoleId;
    
    @Column(name = "rum_user_id", nullable = false)
    private Long rumUserId;
    
    @Column(name = "rum_state", nullable = true)
    private Integer rumState;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_timestamp", nullable = true)
    private Date createTimestamp;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "change_timestamp", nullable = true)
    private Date changeTimestamp;
    
    @Column(name = "change_username", nullable = true, length = 200)
    private String changeUsername;

    @ManyToOne
    @JoinColumn(name = "rum_user_id", referencedColumnName = "u_id", insertable = false, updatable = false)
    private AmUserProfile user;

    @ManyToOne
    @JoinColumn(name = "rum_role_id", referencedColumnName = "role_id", insertable = false, updatable = false)
    private LuRoles roles;

    public AmUserProfile getUser() {
        return user;
    }

    public void setUser(AmUserProfile user) {
        this.user = user;
    }

    public LuRoles getRoles() {
        return roles;
    }

    public void setRoles(LuRoles roles) {
        this.roles = roles;
    }

    public Long getRumId() {
        return rumId;
    }

    public void setRumId(Long rumId) {
        this.rumId = rumId;
    }

    public Long getRumRoleId() {
        return rumRoleId;
    }

    public void setRumRoleId(Long rumRoleId) {
        this.rumRoleId = rumRoleId;
    }

    public Long getRumUserId() {
        return rumUserId;
    }

    public void setRumUserId(Long rumUserId) {
        this.rumUserId = rumUserId;
    }

    public Integer getRumState() {
        return rumState;
    }

    public void setRumState(Integer rumState) {
        this.rumState = rumState;
    }

    public Date getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(Date createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public Date getChangeTimestamp() {
        return changeTimestamp;
    }

    public void setChangeTimestamp(Date changeTimestamp) {
        this.changeTimestamp = changeTimestamp;
    }

    public String getChangeUsername() {
        return changeUsername;
    }

    public void setChangeUsername(String changeUsername) {
        this.changeUsername = changeUsername;
    }

    @Override
    public Object getId() {
        return this.rumId;
    }

    @Override
    public void setId(Object key) {

    }

    @Override
    public String getSequenceIdQuery() {
        return "select nextval('app_monitor.role_user_map_rum_id_seq')";
    }

    @Override
    public Object getNewID() {
        return null;
    }

    @Override
    public String getFullActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortListNamedQ() {
        return null;
    }

    @Override
    public int getDBEntityID() {
        return 0;
    }
}
