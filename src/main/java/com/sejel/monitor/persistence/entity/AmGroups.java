package com.sejel.monitor.persistence.entity;

import com.sejel.common.jpa.JPAAbstractRoot;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "am_groups", schema = "app_monitor", catalog = "tdcdb")
public class AmGroups extends JPAAbstractRoot implements Serializable {
    @Id
    @Column(name = "grp_id", nullable = false)
    private Long grpId;
    
    @Column(name = "grp_name", nullable = true, length = 200)
    private String grpName;
    
    @Column(name = "grp_sys_id", nullable = false)
    private Long grpSysId;
    
    @Column(name = "grp_events_count", nullable = true)
    private Integer grpEventsCount;
    
    @Column(name = "grp_state", nullable = true)
    private Integer grpState;

    @Column(name = "grp_style", nullable = true, length = 200)
    private String grpStyle;


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_timestamp", nullable = true)
    private Date createTimestamp;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "change_timestamp", nullable = true)
    private Date changeTimestamp;

    @Column(name = "change_username", nullable = true, length = 200)
    private String changeUsername;

    @ManyToOne
    @JoinColumn(name = "grp_sys_id", referencedColumnName = "sys_id", insertable = false, updatable = false)
    private AmSystems system;

    @OneToMany
    @JoinColumn(name = "kpi_grp_id", referencedColumnName = "grp_id", insertable = false, updatable = false)
    private List<AmKpis> kpisList;

    public String getGrpStyle() {
        return grpStyle;
    }

    public void setGrpStyle(String grpStyle) {
        this.grpStyle = grpStyle;
    }

    public AmSystems getSystem() {
        return system;
    }

    public void setSystem(AmSystems system) {
        this.system = system;
    }

    public List<AmKpis> getKpisList() {
        return kpisList;
    }

    public void setKpisList(List<AmKpis> kpisList) {
        this.kpisList = kpisList;
    }

    public Long getGrpId() {
        return grpId;
    }

    public void setGrpId(Long grpId) {
        this.grpId = grpId;
    }

    public String getGrpName() {
        return grpName;
    }

    public void setGrpName(String grpName) {
        this.grpName = grpName;
    }

    public Long getGrpSysId() {
        return grpSysId;
    }

    public void setGrpSysId(Long grpSysId) {
        this.grpSysId = grpSysId;
    }

    public Integer getGrpEventsCount() {
        return grpEventsCount;
    }

    public void setGrpEventsCount(Integer grpEventsCount) {
        this.grpEventsCount = grpEventsCount;
    }

    public Integer getGrpState() {
        return grpState;
    }

    public void setGrpState(Integer grpState) {
        this.grpState = grpState;
    }

    public Date getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(Date createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public Date getChangeTimestamp() {
        return changeTimestamp;
    }

    public void setChangeTimestamp(Date changeTimestamp) {
        this.changeTimestamp = changeTimestamp;
    }

    public String getChangeUsername() {
        return changeUsername;
    }

    public void setChangeUsername(String changeUsername) {
        this.changeUsername = changeUsername;
    }

    @Override
    public Object getId() {
        return this.grpId;
    }

    @Override
    public void setId(Object key) {

    }

    @Override
    public String getSequenceIdQuery() {
        return "select nextval('app_monitor.am_groups_grp_id_seq')";
    }

    @Override
    public Object getNewID() {
        return null;
    }

    @Override
    public String getFullActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortActiveListNamedQ() {
        return null;
    }

    @Override
    public String getShortListNamedQ() {
        return null;
    }

    @Override
    public int getDBEntityID() {
        return 0;
    }
}
