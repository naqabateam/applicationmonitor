function placeHijri(inputElement, hijriElement) {
    if (hijriElement == null) {
        hijriElement = inputElement.parentElement.getElementsByClassName("hijripicker")[0];
    }
    var pickers=document.getElementsByClassName("hijripicker");
    for(counter=0;counter<pickers.length;counter++){
        pickers[counter].style.display="none";

    }

    hijriElement.style.display = 'block';
    var calendarWidth = hijriElement.clientWidth,
        calendarHeight = hijriElement.clientHeight,
        visualPadding = 10,
        windowWidth = window.innerWidth,
        windowHeight = window.innerHeight,
        scrollTop = window.scrollY;

    //var offset = this.component ? this.component.parent().offset() : this.element.offset();
    var height = inputElement.clientHeight;
    var width = inputElement.clientWidth;
    var left = inputElement.offsetLeft;
    var top = inputElement.offsetHeight;

    if (!width) width = 0;

    hijriElement.classList.remove("hijripicker-orient-top");
    hijriElement.classList.remove("hijripicker-orient-bottom");
    hijriElement.classList.remove("hijripicker-orient-right");
    hijriElement.classList.remove("hijripicker-orient-left");

    // auto x orientation is best-placement: if it crosses a window
    // edge, fudge it sideways

    if (hijriElement.classList.contains('hijripicker-rtl')) {

        // Default to right
        hijriElement.classList.add('hijripicker-orient-right');
        left -= calendarWidth - width;
        //if(left < 0) left = left * (-1);
    } else {

        // Default to left
        hijriElement.classList.add('hijripicker-orient-left');
        if (inputElement.offsetLeft < 0)
            left -= inputElement.offsetLeft - visualPadding;
        else if (inputElement.offsetLeft + calendarWidth > windowWidth)
            left = windowWidth - calendarWidth - visualPadding;
    }

    // auto y orientation is best-situation: top or bottom, no fudging,
    // decision based on which shows more of the calendar
    var yorient = 'auto',
        top_overflow, bottom_overflow;
    if (yorient === 'auto') {
        top_overflow = -scrollTop + inputElement.offsetTop - calendarHeight;
        bottom_overflow = scrollTop + windowHeight - (inputElement.offsetTop + height + calendarHeight);
        if (Math.max(top_overflow, bottom_overflow) === bottom_overflow)
            yorient = 'top';
        else
            yorient = 'bottom';
    }
    hijriElement.classList.add('hijripicker-orient-' + yorient);
    /*if (yorient === 'top')
        top += height;
    else {
        //if(hijriElement.css('padding-top'))	top -= calendarHeight + parseInt(hijriElement.css('padding-top'));
        top -= calendarHeight + 40;
    }*/

    hijriElement.style.left = left + 'px';
    hijriElement.style.top = top + 'px';

}

function fillHijriDate(dateElement, date) {
    calendar = dateElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;

    //inputHijri = dateElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.firstChild;

    inputHijri = calendar.parentElement.firstChild;

    //hijriElement = document.getElementsByClassName("hijriInput")[0];
    hijriElement = calendar.parentElement.getElementsByClassName("hijripicker")[0];
    //hijriElement = dateElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.getElementsByClassName("hijripicker")[0];
    inputHijri.value = date;

    event = new UIEvent("change", {
        "view": window,
        "bubbles": true,
        "cancelable": true
    });
    inputHijri.dispatchEvent(event);

    inputHijri.classList.add("ui-state-filled");
    hijriElement.style.display = 'none';
}

function toggleHijri(inputHijri, hijriElement) {
    if (hijriElement.style.display == 'block') hijriElement.style.display = 'none';
    else placeHijri(inputHijri, hijriElement);
}

function toggleHijri(anchorElement) {
    inputHijri = anchorElement.parentElement.firstChild;
    hijriElement = anchorElement.parentElement.getElementsByClassName("hijripicker")[0];
    if (!hijriElement) hijriElement = anchorElement.parentElement;
    if (hijriElement.style.display == 'block') hijriElement.style.display = 'none';
    else placeHijri(inputHijri, hijriElement);
}

$(document).ready(function () {
    $('html').click(function (e) {
        if (e.target.className.indexOf('hijriInput') == -1 && e.target.outerHTML.indexOf('option') != 1) {
            if (e.target.className.indexOf('hijriNav') == -1) {
                $('.hijripicker').css('display', 'none');
            }
        }
    });

});