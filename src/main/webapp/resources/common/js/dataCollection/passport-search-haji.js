$(document).ready(function () {
  var e = "";
  var k = "";
  var d = 2;
  var a = null;
  var b = null;
  var g = null;
  $(document).keypress(function (n) {
    var l = n.currentTarget.activeElement.nodeName;
    var m = String.fromCharCode(n.which);
    if (l != "BODY") {
      return
    }
    e = e + m;
    if (a == null) {
      f()
    }
  });

  function h() {
    e = ""
  }

  function i(m, n, l) {
    j(i18n.Processing, "warning");
    searchForHajiAuto([{name: "passportMRZ", value: m}])
  }

  function j(n, m) {
    var l = $("#passportCaptureStatus");
    l.attr("class", "alert alert-" + m);
    l.html(n)
  }

  function c() {
    window.clearInterval(a);
    if (e.length < 60) {
      h();
      a = null;
      j(i18n.readyForScan, "info");
      return
    }
    g = new Date();
    try {
      i(e, b, g)
    } catch (l) {
      j(i18n.thereWasAnErrorTryAgain, "danger")
    }
    h();
    a = null
  }

  function f() {
    j(i18n.Processing, "warning");
    a = setInterval(function () {
      c()
    }, d * 1000);
    b = new Date();
    g = null
  }

  $(document).focus(function () {
    j(i18n.readyForScan, "info")
  });
  $(document).click(function (m) {
    var l = m.currentTarget.activeElement.nodeName;
    if (l != "BODY") {
      j(i18n.clickHereToGetReadyForScan, "warning");
      return
    }
    j(i18n.readyForScan, "info")
  });
  $(document).focusout(function (l) {
    j(i18n.clickHereToGetReadyForScan, "warning")
  })
});

function autoSearchBlockUI() {
  $.blockUI({
    message: '<i class="fa fa-spinner fa-5x fa-spin"></i>',
    css: {
      left: "50%",
      width: "4%",
      border: "0px solid transparent",
      cursor: "wait",
      backgroundColor: "transparent"
    },
    overlayCSS: {backgroundColor: "transparent", opacity: 0, cursor: "wait"}
  })
}

function autoSearchUnblockUI() {
  $.unblockUI();
  $(document).focus()
};