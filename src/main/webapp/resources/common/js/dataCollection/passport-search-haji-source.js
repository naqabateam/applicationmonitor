/**
 * Created by nmarkaji on 15/02/2017.
 */


$(document).ready(function () {
  var capturedData = "";

  var parsedData = "";

  var maxCapturePeriod = 2; // 2 sec
  var timer = null; // timer
  var startTime = null;
  var endTime = null;

  // ctrl key is not allowed in this page
  /*jQuery(document).bind("keyup keydown", function (e) {
   if (e.ctrlKey) {
   e.preventDefault()
   return false;
   }
   });*/

  $(document).keypress(function (event) {

    var elementNode = event.currentTarget.activeElement.nodeName;
    var key = String.fromCharCode(event.which);

    if (elementNode != 'BODY') {
      return;
    }
    capturedData = capturedData + key;
    if (timer == null) {
      startTimer();
    }
  });

  function resetDataCapture() {
    capturedData = "";
  }

  /// START Data parsing
  function attemptToParse(capturedData, startTimestamp, endTimestamp) {
    updateCaptureStatus(i18n["Processing"], "warning")
    readPassportData([{name: 'passportMRZ', value: capturedData}]);
  }

  // success | info | warning | dagner
  function updateCaptureStatus(msg, type) {
    var $msg = $("#passportCaptureStatus");
    $msg.attr("class", "alert alert-" + type);
    $msg.html(msg);
  }

  /// START : Timer related functionality
  function resetTimer() {
    window.clearInterval(timer); // self clear
    if (capturedData.length < 60) {
      resetDataCapture();
      timer = null;
      // updateCaptureStatus(i18n["readyForScan"], "info");
      updateCaptureStatus(i18n["thereWasAnErrorTryAgain"], "danger");
      resetPassportData();
      return;
    }

    endTime = new Date();
    try {
      attemptToParse(capturedData, startTime, endTime);
    } catch (ex) {
      updateCaptureStatus(i18n["thereWasAnErrorTryAgain"], "warning");
    }
    resetDataCapture();
    timer = null;
  }

  function startTimer() {
    updateCaptureStatus(i18n["Processing"], "warning");
    timer = setInterval(function () {
      resetTimer()
    }, maxCapturePeriod * 1000);
    startTime = new Date();
    endTime = null;
  }

  $(document).focus(function () {
    updateCaptureStatus(i18n["readyForScan"], "info")
  });

  $(document).click(function (event) {
    var elementNode = event.currentTarget.activeElement.nodeName;
    if (elementNode != 'BODY') {
      updateCaptureStatus(i18n["clickHereToGetReadyForScan"], "warning");
      return;
    }
    updateCaptureStatus(i18n["readyForScan"], "info");
  });

  $(document).focusout(function (event) {
    updateCaptureStatus(i18n["clickHereToGetReadyForScan"], "warning");
  });
});

function autoSearchBlockUI() {
  $.blockUI({
    message: '<i class="fa fa-spinner fa-5x fa-spin"></i>',
    css: {
      left: '50%',
      width: '4%',
      border: '0px solid transparent',
      cursor: 'wait',
      backgroundColor: 'transparent'
    },
    overlayCSS: {
      backgroundColor: 'transparent',
      opacity: 0.0,
      cursor: 'wait'
    }
  });
}

function autoSearchUnblockUI() {
  $.unblockUI();
  $(document).focus();
}