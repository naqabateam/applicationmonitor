var initiateCalendar = function() {
    $(".calendar-input-future").calendarsPicker(
        $.extend({
                calendar: $.calendars.instance('ummalqura', lang),
                localNumbers: false,
                showOnFocus: true,
                minDate: 0,
                prevText: '&lt;', // Text for the previous month command
                nextText: '&gt;', // Text for the next month command //
                //pickerClass: 'ui-datepicker',
                dateFormat: 'dd/mm/yyyy',
                useMouseWheel: false,
                renderer: $.calendarsPicker.themeRollerRenderer
            },
            $.calendarsPicker.regionalOptions['en-GB'])
    );
    $(".calendar-input-past").calendarsPicker(
        $.extend({
                calendar: $.calendars.instance('ummalqura', lang),
                localNumbers: false,
                showOnFocus: true,
                maxDate: -1,
                prevText: '&lt;', // Text for the previous month command
                nextText: '&gt;', // Text for the next month command //
                //pickerClass: 'ui-datepicker',
                dateFormat: 'dd/mm/yyyy',
                useMouseWheel: false,
                renderer: $.calendarsPicker.themeRollerRenderer
            },
            $.calendarsPicker.regionalOptions['en-GB'])
    );
    $(".calendar-input").calendarsPicker(
        $.extend({
                calendar: $.calendars.instance('ummalqura', lang),
                localNumbers: false,
                showOnFocus: true,
                prevText: '&lt;', // Text for the previous month command
                nextText: '&gt;', // Text for the next month command //
                //pickerClass: 'ui-datepicker',
                dateFormat: 'dd/mm/yyyy',
                useMouseWheel: false,
                renderer: $.calendarsPicker.themeRollerRenderer
            },
            $.calendarsPicker.regionalOptions['en-GB'])
    );
    $('.is-calendarsPicker').on('change', function () {
        if(this.value == '') {
            this.classList.remove('ui-state-filled');
        } else {
            this.classList.add('ui-state-filled');
        }
    });
    $('.calendar-input-future,.calendar-input-past,.calendar-input').attr('readonly', 'readonly');
}